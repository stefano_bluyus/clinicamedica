﻿using ClinicaMedica.Data;
using ClinicaMedica.Models;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PagedList;

namespace ClinicaMedica.Controllers
{
    [Authorize]
    public class CidController : Controller
    {
        private Context db = new Context();

        // GET: Cid
        [Authorize(Roles = "Supervisor,Médico")]
        public ActionResult Index(string searchString, int? page)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            ViewBag.Recurso = "Tabelas > Cids";
            ViewBag.CampoBusca = "Código";

            if (searchString == null)
            {
                ViewBag.Busca = "";
                return View(db.Cid.OrderBy(c => c.Codigo).ToPagedList(pageNumber, pageSize));
            }
            else
            {
                ViewBag.Busca = searchString;
                return View(db.Cid.Where(c => c.Codigo.StartsWith(searchString)).OrderBy(c => c.Codigo).ToPagedList(pageNumber, pageSize));
            }     
        }

        // GET: Cid/Create
        [Authorize(Roles = "Supervisor")]
        public ActionResult Create()
        {
            ViewBag.Recurso = "Tabelas > Cids > Inclusão";
            return View();
        }

        // POST: Cid/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Supervisor")]
        public ActionResult Create([Bind(Include = "ID,Codigo,Descricao")] Cid cid)
        {
              if (ModelState.IsValid)
               {
                Cid cidExiste = db.Cid.Where(c => c.Codigo.Equals(cid.Codigo)).FirstOrDefault();

                if (cidExiste == null)
                {
                    db.Cid.Add(cid);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Já existe um Cid com este Código");
                }                  
              }      

            return View(cid);
        }

        // GET: Cid/Edit/5
        [Authorize(Roles = "Supervisor")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cid cid = db.Cid.Find(id);
            if (cid == null)
            {
                return HttpNotFound();
            }

            ViewBag.Recurso = "Tabelas > Cids > Alteração";
            return View(cid);
        }

        // POST: Cid/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Supervisor")]
        public ActionResult Edit([Bind(Include = "ID,Codigo,Descricao")] Cid cid)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cid).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cid);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteConfirmed(int id)
        {
            ConsultaCid consulta = db.ConsultaCid.Where(cc => cc.CidId == id).FirstOrDefault();

            if (consulta == null)
            {
                Cid cid = db.Cid.Find(id);
                db.Cid.Remove(cid);
                db.SaveChanges();
                return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Este CID está sendo utilizado e não pode ser excluído." }, JsonRequestBehavior.AllowGet);
            }       
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
