﻿using ClinicaMedica.Data;
using ClinicaMedica.Models;
using PagedList;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ClinicaMedica.Controllers
{
    public class ExameController : Controller
    {
        private Context db = new Context();

        // GET: Exame
        [Authorize(Roles = "Supervisor,Médico,Laboratório")]
        public ActionResult Index(string searchString, int? page)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            ViewBag.Recurso = "Tabelas > Exames";
            ViewBag.CampoBusca = "Código";

            if (searchString == null)
            {
                ViewBag.Busca = "";
                return View(db.Exame.OrderBy(c => c.Codigo).ToPagedList(pageNumber, pageSize));
            }
            else
            {
                ViewBag.Busca = searchString;
                return View(db.Exame.Where(c => c.Codigo.StartsWith(searchString)).OrderBy(c => c.Codigo).ToPagedList(pageNumber, pageSize));
            }
        }

        // GET: Exame/Create
        [Authorize(Roles = "Supervisor")]
        public ActionResult Create()
        {
            ViewBag.Recurso = "Tabelas > Exames > Inclusão";
            return View();
        }

        // POST: Exame/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Supervisor")]
        public ActionResult Create([Bind(Include = "ID,Codigo,Descricao")] Exame exame)
        {
            if (ModelState.IsValid)
            {
                Exame exameExiste = db.Exame.Where(e => e.Codigo.Equals(exame.Codigo)).FirstOrDefault();

                if (exameExiste == null)
                {
                    db.Exame.Add(exame);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Já existe um Exame com este Código");
                }
            }

            return View(exame);
        }

        // GET: Exame/Edit/5
        [Authorize(Roles = "Supervisor")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Exame exame = db.Exame.Find(id);
            if (exame == null)
            {
                return HttpNotFound();
            }

            ViewBag.Recurso = "Tabelas > Exames > Alteração";
            return View(exame);
        }

        // POST: Exame/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Supervisor")]
        public ActionResult Edit([Bind(Include = "ID,Codigo,Descricao")] Exame exame)
        {
            if (ModelState.IsValid)
            {
                db.Entry(exame).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(exame);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteConfirmed(int id)
        {
            ConsultaExame consulta = db.ConsultaExame.Where(cc => cc.ExameId == id).FirstOrDefault();

            if (consulta == null)
            {
                Exame exame = db.Exame.Find(id);
                db.Exame.Remove(exame);
                db.SaveChanges();
                return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Este Exame está sendo utilizado e não pode ser excluído." }, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
