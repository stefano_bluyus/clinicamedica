﻿using ClinicaMedica.Data;
using ClinicaMedica.DTO;
using Rotativa;
using Rotativa.Options;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace ClinicaMedica.Controllers
{
    public class RelatorioController : Controller
    {
        private Context db = new Context();

        [Authorize(Roles = "Médico")]
        public ActionResult Receita(int id)
        {
            RelatorioReceitaDTO query;
            List<MedicamentoConsultaDTO> medicamentos;

            using (Context db1 = new Context())
            {
                query = (from c in db1.Consulta
                         join f in db1.Funcionario on c.FuncionarioId equals f.Id
                         join a in db1.Agenda on c.FuncionarioId equals a.Id
                         join p in db1.Paciente on c.PacienteId equals p.Id
                         where c.Id == id
                         select new RelatorioReceitaDTO
                         {
                             NomeMedico = f.Nome,
                             NomePaciente = p.Nome,
                             DataConsulta = c.DataHora,
                             CRM = a.CRM
                         })
                          .FirstOrDefault();

                medicamentos = (from c in db1.ConsultaMedicamento
                        join m in db1.Medicamento on c.MedicamentoId equals m.ID
                        where c.ConsultaId == id
                        select new MedicamentoConsultaDTO
                        {
                            NomeComercial = m.NomeComercial,
                            NomeGenerico = m.NomeGenerico,
                            Posologia = c.Posologia,
                            ID = m.ID
                        })
                          .ToList();
            }

            query.Medicamentos = medicamentos;

            if (medicamentos.Count == 0)
            {
                return View("Vazio");
            }
            else
            {
                var pdf = new ViewAsPdf
                {
                    ViewName = "Receita",
                    PageSize = Size.A4,
                    IsGrayScale = true,
                    Model = query
                };

                return pdf;

            }
        }

        [Authorize(Roles = "Médico")]
        public ActionResult Atestado(int id)
        {
            RelatorioAtestadoDTO query;
            List<TabelaDTO> cids;

            using (Context db1 = new Context())
            {   
                query = (from c in db1.Consulta
                         join f in db1.Funcionario on c.FuncionarioId equals f.Id
                         join a in db1.Agenda on c.FuncionarioId equals a.Id
                         join p in db1.Paciente on c.PacienteId equals p.Id
                         where c.Id == id
                         select new RelatorioAtestadoDTO
                         {
                             NomeMedico = f.Nome,
                             NomePaciente = p.Nome,
                             DataConsulta = c.DataHora,
                             CRM = a.CRM
                         })
                          .FirstOrDefault();

                cids = (from c in db1.ConsultaCid
                        join ci in db1.Cid on c.CidId equals ci.ID
                        where c.ConsultaId == id
                        select new TabelaDTO
                        {
                            Codigo = ci.Codigo,
                            Descricao = ci.Descricao,
                            ID = ci.ID
                        })
                          .ToList();
            }

            query.Cids = cids;

            if (cids.Count == 0)
            {
                return View("Vazio");
            }
            else
            {
                var pdf = new ViewAsPdf
                {
                    ViewName = "Atestado",
                    PageSize = Size.A4,
                    IsGrayScale = true,
                    Model = query
                };

                return pdf;
            }
        }

        [Authorize(Roles = "Médico")]
        public ActionResult Exame(int id)
        {
            RelatorioExameDTO query;
            List<ExameConsultaDTO> exames;

            using (Context db1 = new Context())
            {
                query = (from c in db1.Consulta
                         join f in db1.Funcionario on c.FuncionarioId equals f.Id
                         join a in db1.Agenda on c.FuncionarioId equals a.Id
                         join p in db1.Paciente on c.PacienteId equals p.Id
                         where c.Id == id
                         select new RelatorioExameDTO
                         {
                             NomeMedico = f.Nome,
                             NomePaciente = p.Nome,
                             DataConsulta = c.DataHora,
                             CRM = a.CRM
                         })
                          .FirstOrDefault();

                exames = (from c in db1.ConsultaExame
                        join e in db1.Exame on c.ExameId equals e.ID
                        where c.ConsultaId == id
                        select new ExameConsultaDTO
                        {
                            Codigo = e.Codigo,
                            Descricao = e.Descricao,
                            ID = e.ID,
                            Login = c.Login,
                            Senha = c.Senha,
                            NumeroPedido = c.NumeroPedido
                        })
                          .ToList();
            }

            query.Exames = exames;

            if (exames.Count == 0)
            {
                return View("Vazio");
            }
            else
            {
                var pdf = new ViewAsPdf
                {
                    ViewName = "Exame",
                    PageSize = Size.A4,
                    IsGrayScale = true,
                    Model = query
                };

                return pdf;
            }
        }

        [Authorize(Roles = "Recepção")]
        public ActionResult HistoricoConsulta(int id)
        {
            RelatorioHistoricoConsultaPacienteDTO query;
            List<RelatorioHistoricoConsultaDTO> consultas;

            using (Context db1 = new Context())
            {
                query = (from p in db1.Paciente
                         where p.Id == id
                         select new RelatorioHistoricoConsultaPacienteDTO
                         {
                             NomePaciente = p.Nome,
                             DataNascimento = p.DataNascimento,
                             NomeMae = p.NomeMae
                         })
                         .FirstOrDefault();

                consultas = (from c in db1.Consulta
                             join f in db1.Funcionario on c.FuncionarioId equals f.Id
                             join a in db1.Agenda on c.FuncionarioId equals a.Id
                             join e in db1.Especialidade on a.EspecialidadeId equals e.ID
                             where c.PacienteId == id
                             select new RelatorioHistoricoConsultaDTO
                             {
                                 NomeMedico = f.Nome,
                                 DataConsulta = c.DataHora,
                                 Especialidade = e.Descricao
                             })
                             .OrderBy(c => c.DataConsulta)
                             .ToList();
            }

            query.Consultas = consultas;

            if (consultas.Count == 0)
            {
                return View("Vazio");
            }
            else
            {
                var pdf = new ViewAsPdf
                {
                    ViewName = "HistoricoConsulta",
                    PageSize = Size.A4,
                    IsGrayScale = true,
                    Model = query
                };

                return pdf;

            }
        }        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}