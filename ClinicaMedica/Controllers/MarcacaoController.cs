﻿using ClinicaMedica.Data;
using ClinicaMedica.DTO;
using ClinicaMedica.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace ClinicaMedica.Controllers
{
    public class MarcacaoController : Controller
    {
        private Context db = new Context();

        [Authorize(Roles = "Recepção")]
        public ActionResult SelecionarPaciente(string operacao)
        {
            ViewBag.Operacao = operacao;
            ViewBag.CampoBusca = "Nome do paciente";

            if (operacao == "M")
            {
                ViewBag.Recurso = "Atendimento > Marcar consulta";
            }
            else
            {
                ViewBag.Recurso = "Atendimento > Desmarcar consulta";
            }

            ViewBag.Recurso = "Atendimento > Marcar consulta";
            return View("SelecionarPaciente");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Recepção")]
        public ActionResult SelecionarPaciente(int Id, string operacao)
        {
            return RedirectToAction("MarcarConsulta", "Marcacao", new { PacienteId = Id, Operacao = operacao });
        }

        [Authorize(Roles = "Recepção")]
        public ActionResult MarcarConsulta(int PacienteId, string Operacao)
        {
            MarcarConsultaDTO parametros = new MarcarConsultaDTO();

            using (var context = new Context())
            {
                List<SelectListItem> especialidades = context.Especialidade.AsNoTracking()
                    .OrderBy(e => e.Descricao)
                        .Select(e =>
                        new SelectListItem
                        {
                            Value = e.ID.ToString(),
                            Text = e.Descricao
                        }).ToList();

                parametros.DataConsulta = DateTime.Now;
                parametros.Especialidades = especialidades;
                parametros.PacienteId = PacienteId;
            }

            if (Operacao == "M")
            {
                ViewBag.Recurso = "Atendimento > Marcar consulta";
            }
            else
            {
                ViewBag.Recurso = "Atendimento > Desmarcar consulta";
            }

            ViewBag.Operacao = Operacao;
            return View("MarcarConsulta", parametros);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Recepção")]
        public ActionResult MarcarConsulta(int ConsultaId, int PacienteId, string Operacao)
        {
            bool Alterar = true;
            ViewBag.MsgErro = "";

            MarcarConsultaDTO parametros = new MarcarConsultaDTO();

            using (var context = new Context())
            {
                List<SelectListItem> especialidades = context.Especialidade.AsNoTracking()
                    .OrderBy(e => e.Descricao)
                        .Select(e =>
                        new SelectListItem
                        {
                            Value = e.ID.ToString(),
                            Text = e.Descricao
                        }).ToList();

                parametros.Especialidades = especialidades;
                parametros.PacienteId = PacienteId;
            }

            using (var context = new Context())
            {
                Consulta consulta = context.Consulta.Find(ConsultaId);
                parametros.DataConsulta = consulta.DataHora;

                if (Operacao == "M")
                {
                    consulta.PacienteId = PacienteId;
                }
                else
                {
                    ConsultaCid consultacid = context.ConsultaCid.Where(c => c.ConsultaId == ConsultaId).FirstOrDefault();

                    if (consultacid != null)
                    {
                        Alterar = false;
                        parametros.EspecialidadeId = context.Agenda.Where(a => a.Id == consulta.FuncionarioId).Select(a => a.EspecialidadeId).FirstOrDefault();
                    }
                    else
                    {
                        consulta.PacienteId = null;
                    }                    
                }                

                if ((ModelState.IsValid) && (Alterar))
                {
                    db.Entry(consulta).State = EntityState.Modified;
                    db.SaveChanges();
                }       
            }

            if (Alterar)
            {
                return RedirectToAction("SelecionarPaciente", "Marcacao", new { operacao = Operacao });
            }
            else
            {
                ViewBag.MsgErro = "Esta consulta já possui Cids lançados e não poderá ser desmarcada.";
                return View("MarcarConsulta", parametros);
            }     
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BuscarConsultas(DateTime dataConsulta, int EspecialidadeId, string Operacao, int PacienteId)
        {
            if (Operacao == "M")
            {
                List<BuscaConsultaDTO> fcbVmList;

                DateTime dataConsulta30 = dataConsulta.AddDays(30);

                using (var ctx = new Context())
                {
                    fcbVmList = (from c in ctx.Consulta
                                 join f in ctx.Funcionario on c.FuncionarioId equals f.Id
                                 join a in ctx.Agenda on c.FuncionarioId equals a.Id
                                 where c.DataHora >= dataConsulta
                                 where c.DataHora <= dataConsulta30
                                 where a.EspecialidadeId == EspecialidadeId
                                 where c.PacienteId == null

                                 select new BuscaConsultaDTO
                                 {
                                     Data = c.DataHora,
                                     NomeMedico = f.Nome,
                                     Id = c.Id
                                 })
                                   .ToList();
                }

                return Json(fcbVmList, JsonRequestBehavior.AllowGet);
            }
            else
            {
                List<BuscaConsultaDTO> fcbVmList;

                DateTime dataConsulta30 = dataConsulta.AddDays(1);

                using (var ctx = new Context())
                {
                    fcbVmList = (from c in ctx.Consulta
                                 join f in ctx.Funcionario on c.FuncionarioId equals f.Id
                                 join a in ctx.Agenda on c.FuncionarioId equals a.Id
                                 where c.DataHora >= dataConsulta
                                 where c.DataHora < dataConsulta30
                                 where a.EspecialidadeId == EspecialidadeId
                                 where c.PacienteId == PacienteId

                                 select new BuscaConsultaDTO
                                 {
                                     Data = c.DataHora,
                                     NomeMedico = f.Nome,
                                     Id = c.Id
                                 })
                                   .ToList();
                }

                return Json(fcbVmList, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult BuscarPacientes(string Nome)
        {
            using (var ctx = new Context())
            {
                var dados = ctx.Paciente.Where(p => p.Nome.StartsWith(Nome)).Select(p => new { p.Nome, p.NomeMae, p.DataNascimento, p.CPF, p.Id }).ToList();
                return Json(dados, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
