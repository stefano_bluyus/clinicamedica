﻿using ClinicaMedica.Data;
using ClinicaMedica.DTO;
using ClinicaMedica.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace ClinicaMedica.Controllers
{
    public class LaboratorioController : Controller
    {
        private Context db = new Context();

        [Authorize(Roles = "Laboratório")]
        public ActionResult Entregar()
        {
            ViewBag.Recurso = "Laboratório > Entrega de exames";
            return View("Entregar");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Laboratório")]
        public ActionResult Entregar(EntregaExameDTO exame)
        {
            if (ModelState.IsValid)
            {
                using (Context ctx = new Context())
                {
                    ConsultaExame exameBD = ctx.ConsultaExame.Where(ce => ce.NumeroPedido == exame.NumeroPedido).FirstOrDefault();

                    if (exameBD == null)
                    {
                        ViewBag.Message = "Não foi encontrado nenhum pedido de exame com este número";
                    }
                    else
                    {
                        if ((exameBD.Realizado == true) && (exame.Sobrescrever == false))
                        {
                            ViewBag.Message = "Este exame já foi entregue";
                        }
                        else
                        {
                            bool folderExists = Directory.Exists(Server.MapPath("Resultados"));

                            if (!folderExists)
                            {
                                Directory.CreateDirectory(Server.MapPath("Resultados"));
                            }

                            if (exame.File != null && exame.File.ContentLength > 0)
                            {
                                try
                                {
                                    string path = Path.Combine(Server.MapPath("Resultados"),
                                                               exame.NumeroPedido.ToString());

                                    if (System.IO.File.Exists(path + ".PDF"))
                                    {
                                        System.IO.File.Delete(path + ".PDF");
                                    }

                                    if (System.IO.File.Exists(path + ".GIF"))
                                    {
                                        System.IO.File.Delete(path + ".GIF");
                                    }

                                    if (System.IO.File.Exists(path + ".PNG"))
                                    {
                                        System.IO.File.Delete(path + ".PNG");
                                    }

                                    if (System.IO.File.Exists(path + ".JPEG"))
                                    {
                                        System.IO.File.Delete(path + ".JPEG");
                                    }

                                    if (System.IO.File.Exists(path + ".JPG"))
                                    {
                                        System.IO.File.Delete(path + ".JPG");
                                    }

                                    path = path + Path.GetExtension(exame.File.FileName);

                                    exame.File.SaveAs(path);

                                    ConsultaExame exameAlt = ctx.ConsultaExame.Find(exameBD.ConsultaId, exameBD.ExameId);
                                    exameAlt.Realizado = true;
                                    exameAlt.DataLiberacao = DateTime.Now;
                                    ctx.Entry(exameAlt).State = EntityState.Modified;
                                    ctx.SaveChanges();
                                }
                                catch (Exception ex)
                                {
                                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                                }
                            }
                        }

                    }
                }
            }

            return View();
        }

        public ActionResult Resultado(int id, string login, string senha)
        {
            using (db)
            {
                var obj = db.ConsultaExame.Where(ce => ce.Login.Equals(login) && ce.Senha.Equals(senha) && ce.NumeroPedido == id).FirstOrDefault();

                if (obj != null)
                {
                    List<ResultadoDTO> query;

                    using (Context db1 = new Context())
                    {
                        query = (from c in db1.Consulta
                                 join ce in db1.ConsultaExame on c.Id equals ce.ConsultaId
                                 join e in db1.Exame on ce.ExameId equals e.ID
                                 join f in db1.Funcionario on c.FuncionarioId equals f.Id
                                 join a in db1.Agenda on c.FuncionarioId equals a.Id
                                 join p in db1.Paciente on c.PacienteId equals p.Id
                                 where ce.NumeroPedido == id
                                 select new ResultadoDTO
                                 {
                                     NomeMedico = f.Nome + " (CRM: " + a.CRM + ")",
                                     DataPrescricao = ce.DataPedido,
                                     DataLiberacao = ce.DataLiberacao,
                                     NomeExame = e.Descricao,
                                     NumeroPedido = id,
                                     Senha = ce.Senha
                                 })
                                  .ToList();

                        if (query[0].DataLiberacao == DateTime.MinValue)
                        {
                            query[0].Status = "Aguardando";
                        }
                        else
                        {
                            query[0].Status = "Liberado";
                        }

                        return View("Resultado", query);
                    }
                }
                else
                {
                    return RedirectToAction("LogarResultado", "Laboratorio", new { });
                }
            }
        }

        [Authorize(Roles = "Médico")]
        public ActionResult ResultadoMedico(int ConsultaId)
        {
            List<ResultadoDTO> query;

            using (Context db1 = new Context())
            {
                query = (from c in db1.Consulta
                         join ce in db1.ConsultaExame on c.Id equals ce.ConsultaId
                         join e in db1.Exame on ce.ExameId equals e.ID
                         join f in db1.Funcionario on c.FuncionarioId equals f.Id
                         join a in db1.Agenda on c.FuncionarioId equals a.Id
                         join p in db1.Paciente on c.PacienteId equals p.Id
                         where c.Id == ConsultaId
                         select new ResultadoDTO
                         {
                             NomeMedico = f.Nome + " (CRM: " + a.CRM + ")",
                             DataPrescricao = ce.DataPedido,
                             DataLiberacao = ce.DataLiberacao,
                             NomeExame = e.Descricao,
                             NumeroPedido = ce.NumeroPedido,
                             Senha = ce.Senha
                         })
                          .ToList();

                for (int i = 0; i < query.Count; i++)
                {
                    if (query[i].DataLiberacao == DateTime.MinValue)
                    {
                        query[i].Status = "Aguardando";
                    }
                    else
                    {
                        query[i].Status = "Liberado";
                    }
                }                
            }

            return View("Resultado", query);
        }

        [HttpPost]
        public ActionResult LogarResultado(LoginDTO exame)
        {
            if (ModelState.IsValid)
            {
                using (db)
                {
                    var obj = db.ConsultaExame.Where(ce => ce.Login.Equals(exame.Login) && ce.Senha.Equals(exame.Senha)).FirstOrDefault();

                    if (obj != null)
                    {
                        return RedirectToAction("Resultado", "Laboratorio", new { @id = obj.NumeroPedido, @login = obj.Login, @senha = obj.Senha });
                    }
                    else
                    {
                        ModelState.AddModelError("", "Usuário ou senha inválidos");
                        return View();
                    }
                }
            }
            else
            {
                return View();
            }
        }

        public ActionResult LogarResultado()
        {
            return View("LogarResultado");
        }

        public List<UploadFileResult> ListaArquivos()
        {
            List<UploadFileResult> lstArquivos = new List<UploadFileResult>();
            DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath("~/Laboratorio/Resultados"));

            int i = 0;
            foreach (var item in dirInfo.GetFiles())
            {
                lstArquivos.Add(new UploadFileResult()
                {
                    IDArquivo = int.Parse(Path.GetFileNameWithoutExtension(item.Name)),
                    Nome = item.Name,
                    Caminho = dirInfo.FullName + @"\" + item.Name
                });
                i = i + 1;
            }
            return lstArquivos;
        }

        public ActionResult Download(int id, string senha)
        {

            using (db)
            {
                var obj = db.ConsultaExame.Where(ce => ce.Senha.Equals(senha) && ce.NumeroPedido == id).FirstOrDefault();

                if (obj != null)
                {
                    string contentType = "";

                    var arquivos = ListaArquivos();

                    string nomeArquivo = (from arquivo in arquivos
                                          where arquivo.IDArquivo == id
                                          select arquivo.Caminho).First();


                    string extensao = Path.GetExtension(nomeArquivo).ToUpper();
                    string nomeArquivoV = Path.GetFileNameWithoutExtension(nomeArquivo);

                    if (extensao.Equals(".PDF"))
                        contentType = "application/pdf";

                    if (extensao.Equals(".JPG") || extensao.Equals(".GIF") || extensao.Equals(".PNG"))
                        contentType = "application/image";
                    return File(nomeArquivo, contentType, nomeArquivoV + extensao);
                }
                else
                {
                    return RedirectToAction("LogarResultado", "Laboratorio", new { });
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}