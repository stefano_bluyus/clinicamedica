﻿using ClinicaMedica.Data;
using ClinicaMedica.DTO;
using ClinicaMedica.Models;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using PagedList;
using System;

namespace ClinicaMedica.Controllers
{
    public class FuncionarioController : Controller
    {
        private Context db = new Context();

        // GET: Funcionario
        [Authorize(Roles = "Supervisor")]
        public ActionResult Index(string searchString, int? page)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            ViewBag.Recurso = "Recursos > Funcionário";
            ViewBag.CampoBusca = "Nome";

            if (searchString == null)
            {
                ViewBag.Busca = "";
                return View(db.Funcionario.Include(f => f.Agenda).OrderBy(f => f.Nome).ToPagedList(pageNumber, pageSize));
            }
            else
            {
                ViewBag.Busca = searchString;
                return View(db.Funcionario.Include(f => f.Agenda).Where(f => f.Nome.StartsWith(searchString)).OrderBy(f => f.Nome).ToPagedList(pageNumber, pageSize));
            }
        }

        // GET: Funcionario/Create
        [Authorize(Roles = "Supervisor")]
        public ActionResult Create()
        {
            ViewBag.EspecialidadeId = new SelectList(db.Especialidade, "ID", "Descricao");   
            ViewBag.Recurso = "Recursos > Funcionários > Inclusão";
            return View();
        }

        // POST: Funcionario/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Supervisor")]
        public ActionResult Create([Bind(Include = "ID,Nome,Login,Senha,Perfil," +
                                                   "Agenda")] Funcionario funcionario, string EspecialidadeId)
        {
            if (funcionario.Perfil == TipoAcesso.Médico)
            {
                funcionario.Agenda.EspecialidadeId = Int32.Parse(EspecialidadeId);
            }
            else
            {
                funcionario.Agenda = null;
            }

            Funcionario funcionarioExiste = db.Funcionario.Where(f => f.Login.Equals(funcionario.Login)).FirstOrDefault();

            if (funcionarioExiste != null)
            {
                ModelState.AddModelError("", "Já existe um Funcionário com este Login");
            }
            else
            {
                db.Funcionario.Add(funcionario);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(funcionario);
        }

        // GET: Funcionario/Edit/5
        [Authorize(Roles = "Supervisor")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Funcionario funcionario = db.Funcionario.Include(a => a.Agenda).Where(f => f.Id == id).FirstOrDefault();

            if (funcionario == null)
            {
                return HttpNotFound();
            }

            ViewBag.EspecialidadeId = new SelectList(db.Especialidade, "ID", "Descricao");
            ViewBag.Recurso = "Recursos > Funcionários > Alteração";
            return View(funcionario);
        }

        // POST: Funcionario/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Nome,Login,Perfil,Agenda")] Funcionario funcionario, string EspecialidadeId)
        {
            ViewBag.EspecialidadeId = new SelectList(db.Especialidade, "ID", "Descricao");
            Funcionario funcionarioExiste = db.Funcionario.Where(f => f.Login.Equals(funcionario.Login) && f.Id != funcionario.Id).FirstOrDefault();
            int QtdConsulta = 0;
            ViewBag.MsgErro = "";

            if (funcionarioExiste != null)
            {
                ModelState.AddModelError("", "Já existe um Funcionário com este Login");
                return View(funcionario);
            }
            else
            { 
                if (funcionario.Perfil == TipoAcesso.Médico)
                {
                    int EspecialidadeOld = db.Agenda.Where(a => a.Id == funcionario.Id).Select(a => a.EspecialidadeId).First();

                    if (EspecialidadeOld != Int32.Parse(EspecialidadeId))
                    {
                        QtdConsulta = db.Consulta.Where(c => c.FuncionarioId == funcionario.Id && c.PacienteId != null).Select(c => c.Id).First();
                    }

                    if (QtdConsulta > 0)
                    {
                        ViewBag.MsgErro = "A Especialidade não pode ser alterada, pois o médico possui consultas agendadas.";
                        return View(funcionario);
                    }

                    funcionario.Agenda.Id = funcionario.Id;
                    funcionario.Agenda.EspecialidadeId = Int32.Parse(EspecialidadeId);
                    db.Entry(funcionario.Agenda).State = EntityState.Modified;
                }
                else
                {
                    funcionario.Agenda = null;
                }

                string senha = db.Funcionario.Where(f => f.Id == funcionario.Id).Select(f => f.Senha).FirstOrDefault();
                funcionario.Senha = senha;

                db.Entry(funcionario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }                
        }

        // POST: Funcionario/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Consulta consulta = db.Consulta.Where(c => c.FuncionarioId == id).FirstOrDefault();

            if (consulta == null)
            {
                Funcionario funcionario = db.Funcionario.Find(id);
                Agenda agenda = db.Agenda.Find(id);

                if (funcionario.Nome != "supervisor")
                {
                    db.Agenda.Remove(agenda);
                    db.Funcionario.Remove(funcionario);
                    db.SaveChanges();
                    return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, responseText = "O Supervisor do sistema não pode ser excluído" }, JsonRequestBehavior.AllowGet);
                }             
            }
            else
            {
                return Json(new { success = false, responseText = "Este Funcionário possui consultas agendadas e não pode ser excluído" }, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
