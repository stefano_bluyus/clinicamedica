﻿namespace ClinicaMedica.Controllers
{
    public class UploadFileResult
    {
        public int IDArquivo { get; set; }
        public string Nome { get; set; }
        public int Tamanho { get; set; }
        public string Tipo { get; set; }
        public string Caminho { get; set; }
    }
}