﻿using ClinicaMedica.Data;
using ClinicaMedica.DTO;
using ClinicaMedica.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web.Mvc;

namespace ClinicaMedica.Controllers
{
    public class AgendaController : Controller
    {
        private Context db = new Context();

        // GET: Consulta
        [Authorize(Roles = "Supervisor")]
        public ActionResult List(DateTime? DataPesquisa, int? page)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);

            if (DataPesquisa == null)
            {
                ViewBag.Busca = DateTime.Now;
            }
            else
            {
                ViewBag.Busca = (DateTime)DataPesquisa;
            }

            DateTime dt = ViewBag.Busca;
            List<AgendasDTO> query;

            using (var db1 = new Context())
            {    
                query = (from c in db1.Consulta
                         join f in db1.Funcionario on c.FuncionarioId equals f.Id
                         where (DateTime)EntityFunctions.TruncateTime(c.DataHora) == dt
                         select new AgendasDTO {
                             IdMedico = c.FuncionarioId,
                             NomeMedico = f.Nome,
                             Data = (DateTime)EntityFunctions.TruncateTime(c.DataHora)
                         })
                         .Distinct()
                         .OrderBy(cf => cf.Data)
                         .ThenBy(cf => cf.NomeMedico).ToList();     
            }

            ViewBag.Recurso = "Atendimento > Excluir agendas";
            ViewBag.CampoBusca = "Data";
            return View(query.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ExcluirAgenda(DateTime data, int medico)
        {
            DateTime DataFinal = data;
            TimeSpan ts = new TimeSpan(23, 53, 0);
            DataFinal = DataFinal.Add(ts);

            Consulta consulta = db.Consulta.Where(c => c.FuncionarioId == medico && 
                                                   c.DataHora >= data &&
                                                   c.DataHora <= DataFinal &&
                                                   c.PacienteId != null).FirstOrDefault();

            if (consulta == null)
            {
                List<Consulta> consultas = db.Consulta.Where(c => c.FuncionarioId == medico &&
                                                             c.DataHora >= data &&
                                                             c.DataHora <= DataFinal).ToList();

                for (int i = 0; i <= consultas.Count - 1; i++)
                {
                    db.Consulta.Remove(consultas[i]);
                }

                db.SaveChanges();
                return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Esta agenda não pode ser excluída, pois existem consultas agendadas." }, JsonRequestBehavior.AllowGet);
            }
        }


        // GET: Consulta/Create
        [Authorize(Roles = "Supervisor")]
        public ActionResult Create()
        {
            AgendaDTO agenda = new AgendaDTO();

            using (var ctx =  new Context())
            {
                var maxObject = ctx.Consulta.OrderByDescending(c => c.DataHora).Select(c => c.DataHora).FirstOrDefault();

                if (maxObject == DateTime.MinValue)
                {
                    agenda.DataInicio = (DateTime.Today).AddDays(1);
                }
                else
                {
                    agenda.DataInicio = Convert.ToDateTime(maxObject).AddDays(1);
                }
            }

            agenda.DataTermino = agenda.DataInicio.AddDays(7);

            ViewBag.Recurso = "Atendimento > Gerar agendas";
            return View(agenda);
        }

        // POST: Consulta/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Supervisor")]
        public ActionResult Create([Bind(Include = "DataInicio, DataTermino")] AgendaDTO agenda)
        {
            if (ModelState.IsValid)
            {
                // buscar medicos que atendem nos dias da semana
                List<Funcionario> medicosSeg = db.Funcionario.Include(a => a.Agenda).Where(f => f.Agenda.Segunda.Equals(true)).ToList();
                List<Funcionario> medicosTer = db.Funcionario.Include(a => a.Agenda).Where(f => f.Agenda.Terca.Equals(true)).ToList();
                List<Funcionario> medicosQua = db.Funcionario.Include(a => a.Agenda).Where(f => f.Agenda.Quarta.Equals(true)).ToList();
                List<Funcionario> medicosQui = db.Funcionario.Include(a => a.Agenda).Where(f => f.Agenda.Quinta.Equals(true)).ToList();
                List<Funcionario> medicosSex = db.Funcionario.Include(a => a.Agenda).Where(f => f.Agenda.Sexta.Equals(true)).ToList();
                List<Funcionario> medicosSab = db.Funcionario.Include(a => a.Agenda).Where(f => f.Agenda.Sabado.Equals(true)).ToList();
                List<Funcionario> medicosDom = db.Funcionario.Include(a => a.Agenda).Where(f => f.Agenda.Domingo.Equals(true)).ToList();

                List<Funcionario> dia = null;

                for (var day = agenda.DataInicio; day <= agenda.DataTermino; day = day.AddDays(1))
                {
                    switch (day.DayOfWeek)
                    {
                        case DayOfWeek.Sunday:
                            dia = medicosDom;
                            break;

                        case DayOfWeek.Monday:
                            dia = medicosSeg;
                            break;

                        case DayOfWeek.Tuesday:
                            dia = medicosTer;
                            break;

                        case DayOfWeek.Wednesday:
                            dia = medicosQua;
                            break;

                        case DayOfWeek.Thursday:
                            dia = medicosQui;
                            break;

                        case DayOfWeek.Friday:
                            dia = medicosSex;
                            break;

                        case DayOfWeek.Saturday:
                            dia = medicosSab;
                            break;
                    }


                    foreach (Funcionario Medico in dia)
                    {
                        DateTime HoraInicio = Medico.Agenda.HorarioInicio;

                        DateTime today = day;
                        TimeSpan newTime = new TimeSpan(Medico.Agenda.HorarioInicio.Hour, Medico.Agenda.HorarioInicio.Minute, 0);
                        today = today.Date + newTime;

                        for (int i = 0; i < Medico.Agenda.QuantidadeConsultas; i++)
                        {                           
                            Consulta cons = new Consulta();
                            cons.FuncionarioId = Medico.Id;
                            cons.DataHora = today;  
                            db.Consulta.Add(cons);
                            db.SaveChanges();

                           today = today.AddMinutes(Medico.Agenda.TempoConsulta);
                        }
                    }
                }
            }

            return RedirectToAction("Index", "Principal");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
