﻿using ClinicaMedica.Data;
using ClinicaMedica.DTO;
using ClinicaMedica.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;

namespace ClinicaMedica.Controllers
{
    public class ConsultaController : Controller
    {
        private Context db = new Context();

        [Authorize(Roles = "Médico")]
        public ActionResult Index(DateTime? DataPesquisa, int id)
        {
            List<ConsultasMedicoDTO> query;

            if (DataPesquisa == null)
            {
                ViewBag.Busca = DateTime.Now;
            }
            else
            {
                ViewBag.Busca = (DateTime)DataPesquisa;
            }

            DateTime dt = ViewBag.Busca;
            

            using (var db1 = new Context())
            {
                query = (from c in db1.Consulta
                         join p in db1.Paciente on c.PacienteId equals p.Id
                         where c.FuncionarioId == id
                         where (DateTime)EntityFunctions.TruncateTime(c.DataHora) == dt
                         select new ConsultasMedicoDTO
                         {
                             IdConsulta = c.Id,
                             NomePaciente = p.Nome,
                             Horario = c.DataHora
                         })
                         .OrderBy(cf => cf.Horario).ToList();
            }

            ViewBag.Recurso = "Atender";
            ViewBag.CampoBusca = "Data";
            return View(query);
        }

        // GET: Consulta
        public ActionResult GetConsultasMedico()
        {
            var consulta = db.Consulta.Include(c => c.Medico).Include(c => c.Paciente).Where(c => c.PacienteId > 0);
            return View(consulta.ToList());
        }

        // POST: Consulta/Delete/5
        [HttpPost]
        // [ValidateAntiForgeryToken]
        public JsonResult DeleteMedicamento(int MedicamentoId, int ConsultaId)
        {
            ConsultaMedicamento consulta = db.ConsultaMedicamento.Find(ConsultaId, MedicamentoId);

            if (consulta == null)
            {
                return Json("{\"result\" : \"falhou\" }");
            }

            db.ConsultaMedicamento.Remove(consulta);
            db.SaveChanges();

            return Json("{\"result\" : \"ok\" }");
        }

        public JsonResult GetMedicamento(string Medicamento, int Consulta)
        {
            List<MedicamentoDTO> query;

            using (var db1 = new Context())
            {
                query = (from m in db1.Medicamento
                         where !(from cm in db1.ConsultaMedicamento
                                 where cm.ConsultaId == Consulta
                                 select cm.MedicamentoId).Contains(m.ID)
                         where m.NomeGenerico.StartsWith(Medicamento)
                         select new MedicamentoDTO
                         {
                             ID = m.ID,
                             NomeComercial = m.NomeComercial,
                             Descricao = m.NomeGenerico + " - " + m.NomeComercial + " (" + m.Fabricante + ")"
                         }).ToList();

                return Json(query, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult AddMedicamento(int MedicamentoId, string Posologia, int ConsultaId)
        {
            ConsultaMedicamento consulta = db.ConsultaMedicamento.Find(ConsultaId, MedicamentoId);

            if (consulta == null)
            {
                ConsultaMedicamento consulta2 = new ConsultaMedicamento();
                consulta2.MedicamentoId = MedicamentoId;
                consulta2.ConsultaId = ConsultaId;
                consulta2.Posologia = Posologia; 
                db.ConsultaMedicamento.Add(consulta2);
                db.SaveChanges();

                return Json("{\"result\" : \"ok\" }");
            }

            return Json("{\"result\" : \"já tinha\" }");
        }

        // POST: Consulta/Delete/5
        [HttpPost]
       // [ValidateAntiForgeryToken]
        public JsonResult DeleteCid(int CidId, int ConsultaId)
        {
            ConsultaCid consulta = db.ConsultaCid.Find(ConsultaId, CidId);

            if (consulta == null)
            {
                return Json("{\"result\" : \"falhou\" }");
            }

            db.ConsultaCid.Remove(consulta);
            db.SaveChanges();

            return Json("{\"result\" : \"ok\" }");
        }

        public JsonResult AddCid(int CidId, int ConsultaId)
        {
            ConsultaCid consulta = db.ConsultaCid.Find(ConsultaId, CidId);

            if (consulta == null)
            {
                ConsultaCid consulta2 = new ConsultaCid();
                consulta2.CidId = CidId;
                consulta2.ConsultaId = ConsultaId;
                db.ConsultaCid.Add(consulta2);
                db.SaveChanges();

                return Json("{\"result\" : \"ok\" }");
            }

            return Json("{\"result\" : \"já tinha\" }");
        }

        public JsonResult GetCid(string Cid, int Consulta)
        {

            List<TabelaDTO> query;

            using (var db1 = new Context())
            {
                query = (from c in db1.Cid
                            where !(from cc in db1.ConsultaCid
                                   where cc.ConsultaId == Consulta
                                    select cc.CidId).Contains(c.ID)
                         where c.Codigo.StartsWith(Cid)
                select new TabelaDTO
                {
                    ID = c.ID,
                    Codigo = c.Codigo,
                    Descricao = c.Codigo + " - " + c.Descricao
                }).ToList();

                return Json(query, JsonRequestBehavior.AllowGet);
            }
        }

        // POST: Consulta/Delete/5
        [HttpPost]
        // [ValidateAntiForgeryToken]
        public JsonResult DeleteExame(int ExameId, int ConsultaId)
        {
            ConsultaExame consulta = db.ConsultaExame.Find(ConsultaId, ExameId);

            if (consulta == null)
            {
                return Json(new { success = false, responseText = "Falha na exclusão." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (consulta.Realizado == true)
                {
                    return Json(new { success = false, responseText = "Este Exame já foi realizado e não poderá ser excluído." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    db.ConsultaExame.Remove(consulta);
                    db.SaveChanges();
                    return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public string GerarValorAleatorio(int campo)
        {
            bool Existe = true;
            int iSize = 10;
            string validar = "abcdefghijklmnozABCDEFGHIJKLMNOZ1234567890@#$%&*!";

            StringBuilder strbld = new StringBuilder(10);
            Random random = new Random();

            while (Existe == true)
            {
                while (strbld.Length < iSize)
                {
                    strbld.Append(validar[random.Next(validar.Length)]); 
                }

                string resultado = strbld.ToString();
                ConsultaExame LoginExistente;

                if (campo == 1)
                {
                    LoginExistente = db.ConsultaExame.Where(c => c.Login == resultado).FirstOrDefault();
                }
                else
                {
                    LoginExistente = db.ConsultaExame.Where(c => c.Senha == resultado).FirstOrDefault();
                }

                if (LoginExistente != null)
                {
                    strbld.Remove(0, 10);
                }
                else
                {
                    Existe = false;
                }
        }

            return strbld.ToString();
        }
            
        public JsonResult AddExame(int ExameId, int ConsultaId)
        {
            ConsultaExame consulta = db.ConsultaExame.Find(ConsultaId, ExameId);

            if (consulta == null)
            {
                ConsultaExame consulta2 = new ConsultaExame();
                consulta2.ExameId = ExameId;
                consulta2.ConsultaId = ConsultaId;
                consulta2.DataPedido = DateTime.Now;
                consulta2.Login = GerarValorAleatorio(1);
                consulta2.Senha = GerarValorAleatorio(2);
                db.ConsultaExame.Add(consulta2);
                db.SaveChanges();

                return Json("{\"result\" : \"ok\" }");
            }

            return Json("{\"result\" : \"já tinha\" }");
        }

        public JsonResult GetExame(string Exame, int Consulta)
        {

            List<ExameDTO> query;

            using (var db1 = new Context())
            {
                query = (from e in db1.Exame
                         where !(from ce in db1.ConsultaExame
                                 where ce.ConsultaId == Consulta
                                 select ce.ExameId).Contains(e.ID)
                         where e.Codigo.StartsWith(Exame)
                         select new ExameDTO
                         {
                             ID = e.ID,
                             Codigo = e.Codigo,
                             Descricao = e.Codigo + " - " + e.Descricao
                         }).ToList();

                return Json(query, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Consulta/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Consulta consulta = db.Consulta.Include(c => c.Paciente).Where(c => c.Id == id).First();

            if (consulta == null)
            {
                return HttpNotFound();
            }

            List<ConsultaCidDTO> cids = new List<ConsultaCidDTO>();
            List<ConsultaExameDTO> exames = new List<ConsultaExameDTO>();
            List<ConsultaMedicamentoDTO> medicamentos = new List<ConsultaMedicamentoDTO>();

            var query_cid = db.ConsultaCid.Include(c => c.Cid).Where(c => c.ConsultaId == id).ToList();

            ConsultaDTO consultadto = new ConsultaDTO();
            consultadto.DataHoraConsulta = consulta.DataHora;
            consultadto.NomePaciente = consulta.Paciente.Nome;
            consultadto.Id = consulta.Id;
            consultadto.Queixas = consulta.Queixas;

            for (int i = 0; i < query_cid.Count; i++)
            {
                ConsultaCidDTO cid = new ConsultaCidDTO();

                cid.Id = query_cid[i].CidId;
                cid.Cid = query_cid[i].Cid.Codigo;

                cids.Add(cid);
            }

            consultadto.Cids = cids;

            var query_exame = db.ConsultaExame.Include(e => e.Exame).Where(e => e.ConsultaId == id).ToList();

            for (int i = 0; i < query_exame.Count; i++)
            {
                ConsultaExameDTO exame = new ConsultaExameDTO();

                exame.Id = query_exame[i].ExameId;
                exame.Exame = query_exame[i].Exame.Codigo;

                exames.Add(exame);
            }

            consultadto.Exames = exames;

            var query_medicamentos = db.ConsultaMedicamento.Include(m => m.Medicamento).Where(m => m.ConsultaId == id).ToList();

            for (int i = 0; i < query_medicamentos.Count; i++)
            {
                ConsultaMedicamentoDTO medicamento = new ConsultaMedicamentoDTO();

                medicamento.Id = query_medicamentos[i].MedicamentoId;
                medicamento.Medicamento = query_medicamentos[i].Medicamento.NomeComercial;
                medicamento.Posologia = query_medicamentos[i].Posologia;

                medicamentos.Add(medicamento);
            }

            consultadto.Medicamentos = medicamentos;

            return View(consultadto);
        }

        // POST: Consulta/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Queixas")] ConsultaDTO consulta)
        {
            Consulta consultabusca = null;

            if (ModelState.IsValid)
            {
                consultabusca = db.Consulta.Find(consulta.Id);
                consultabusca.Queixas = consulta.Queixas;

                db.Entry(consultabusca).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", new {id = consulta.Id });
            }

            return View(consulta);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
