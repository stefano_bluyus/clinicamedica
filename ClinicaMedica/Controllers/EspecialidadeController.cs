﻿using ClinicaMedica.Data;
using ClinicaMedica.Models;
using PagedList;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ClinicaMedica.Controllers
{
    public class EspecialidadeController : Controller
    {
        private Context db = new Context();

        // GET: Especialidade
        [Authorize(Roles = "Supervisor")]
        public ActionResult Index(string searchString, int? page)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            ViewBag.Recurso = "Recursos > Especialidades";
            ViewBag.CampoBusca = "Descrição";

            if (searchString == null)
            {
                ViewBag.Busca = "";
                return View(db.Especialidade.OrderBy(e => e.Descricao).ToPagedList(pageNumber, pageSize));
            }
            else
            {
                ViewBag.Busca = searchString;
                return View(db.Especialidade.Where(e => e.Descricao.StartsWith(searchString)).OrderBy(e => e.Descricao).ToPagedList(pageNumber, pageSize));
            }
        }

        // GET: Especialidade/Create
        [Authorize(Roles = "Supervisor")]
        public ActionResult Create()
        {
            ViewBag.Recurso = "Recursos > Especialidades > Inclusão";
            return View();
        }

        // POST: Especialidade/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Supervisor")]
        public ActionResult Create([Bind(Include = "ID,Descricao")] Especialidade especialidade)
        {
            if (ModelState.IsValid)
            {
                Especialidade especialidadeExiste = db.Especialidade.Where(e => e.Descricao.Equals(especialidade.Descricao)).FirstOrDefault();

                if (especialidadeExiste == null)
                {
                    db.Especialidade.Add(especialidade);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Já existe uma Especialidade com esta Descrição");
                }
            }

            return View(especialidade);
        }

        // GET: Especialidade/Edit/5
        [Authorize(Roles = "Supervisor")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Especialidade especialidade = db.Especialidade.Find(id);
            if (especialidade == null)
            {
                return HttpNotFound();
            }

            ViewBag.Recurso = "Recursos > Especialidades > Alteração";
            return View(especialidade);
        }

        // POST: Especialidade/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Supervisor")]
        public ActionResult Edit([Bind(Include = "ID,Descricao")] Especialidade especialidade)
        {
            if (ModelState.IsValid)
            {
                Especialidade especialidadeExiste = db.Especialidade.Where(e => e.Descricao.Equals(especialidade.Descricao) && e.ID != especialidade.ID).FirstOrDefault();

                if (especialidadeExiste == null)
                {
                    db.Entry(especialidade).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Já existe uma Especialidade com esta Descrição");
                }
            }
            return View(especialidade);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteConfirmed(int id)
        {
            Agenda consulta = db.Agenda.Where(a => a.EspecialidadeId == id).FirstOrDefault();

            if (consulta == null)
            {
                Especialidade especialidade = db.Especialidade.Find(id);
                db.Especialidade.Remove(especialidade);
                db.SaveChanges();
                return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Esta especialidade está sendo utilizada e não pode ser excluída." }, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
