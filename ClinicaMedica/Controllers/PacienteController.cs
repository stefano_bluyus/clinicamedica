﻿using ClinicaMedica.Data;
using ClinicaMedica.Models;
using PagedList;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ClinicaMedica.Controllers
{
    public class PacienteController : Controller
    {
        private Context db = new Context();

        // GET: Paciente
        [Authorize(Roles = "Recepção, Médico, Laboratório")]
        public ActionResult Index(string searchString, int? page)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            ViewBag.Recurso = "Atendimento > Pacientes";
            ViewBag.CampoBusca = "Nome";

            if (searchString == null)
            {
                ViewBag.Busca = "";
                return View(db.Paciente.OrderBy(p => p.Nome).ToPagedList(pageNumber, pageSize));
            }
            else
            {
                ViewBag.Busca = searchString;
                return View(db.Paciente.Where(p => p.Nome.StartsWith(searchString)).OrderBy(p => p.Nome).ToPagedList(pageNumber, pageSize));
            }
        }

        // GET: Paciente/Create
        [Authorize(Roles = "Recepção")]
        public ActionResult Create()
        {
            ViewBag.Recurso = "Atendimento > Pacientes > Inclusão";
            return View();
        }

        // POST: Paciente/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Recepção")]
        public ActionResult Create([Bind(Include = "Id,Nome,NomeMae,DataNascimento,CPF,Telefone")] Paciente paciente)
        {
            bool PodeGravar = true;

            if (ModelState.IsValid)
            {
                if ((paciente.CPF != null) && (paciente.CPF != ""))
                {
                    Paciente pac = db.Paciente.Where(c => c.CPF == paciente.CPF).FirstOrDefault();

                    if (pac != null)
                    {
                        PodeGravar = false;
                        ModelState.AddModelError("", "Já existe um Paciente com este CPF");
                    }

                }                

                if (PodeGravar)
                {
                    db.Paciente.Add(paciente);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }                      
            }

            return View(paciente);
        }

        // GET: Paciente/Edit/5
        [Authorize(Roles = "Recepção")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Paciente paciente = db.Paciente.Find(id);
            if (paciente == null)
            {
                return HttpNotFound();
            }

            ViewBag.Recurso = "Atendimento > Pacientes > Alteração";
            return View(paciente);
        }

        // POST: Paciente/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Recepção")]
        public ActionResult Edit([Bind(Include = "Id,Nome,NomeMae,DataNascimento,CPF,Telefone")] Paciente paciente)
        {
            bool PodeGravar = true;

            if (ModelState.IsValid)
            {
                if ((paciente.CPF != null) && (paciente.CPF != ""))
                {
                    Paciente pac = db.Paciente.Where(c => c.CPF == paciente.CPF && c.Id != paciente.Id).FirstOrDefault();

                    if (pac != null)
                    {
                        PodeGravar = false;
                        ModelState.AddModelError("", "Já existe um Paciente com este CPF (" + pac.Nome + ")");
                    }

                }

                if (PodeGravar)
                {
                    db.Entry(paciente).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View(paciente);
        }

        // POST: Paciente/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Consulta consulta = db.Consulta.Where(c => c.PacienteId == id).FirstOrDefault();

            if (consulta == null)
            {
                Paciente paciente = db.Paciente.Find(id);
                
                db.Paciente.Remove(paciente);
                db.SaveChanges();
                return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Este Paciente possui consultas agendadas e não pode ser excluído" }, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
