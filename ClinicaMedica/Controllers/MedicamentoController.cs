﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClinicaMedica.Data;
using ClinicaMedica.Models;
using PagedList;

namespace ClinicaMedica.Controllers
{
    public class MedicamentoController : Controller
    {
        private Context db = new Context();

        // GET: Medicamento
        [Authorize(Roles = "Supervisor,Médico")]
        public ActionResult Index(string searchString, int? page)
        {
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            ViewBag.Recurso = "Tabelas > Medicamentos";
            ViewBag.CampoBusca = "Nome comercial";

            if (searchString == null)
            {
                ViewBag.Busca = "";
                return View(db.Medicamento.OrderBy(m => m.NomeComercial).ToPagedList(pageNumber, pageSize));
            }
            else
            {
                ViewBag.Busca = searchString;
                return View(db.Medicamento.Where(m => m.NomeComercial.StartsWith(searchString)).OrderBy(m => m.NomeComercial).ToPagedList(pageNumber, pageSize));
            }
        }

        [HttpPost]
        public JsonResult GetFabricante(string Prefix)
        {
            using (var db1 = new Context())
            {
                var dados = db.Medicamento.Include(m => m.Fabricante).Where(m => m.Fabricante.StartsWith(Prefix)).Select(m => new { m.Fabricante }).ToList();
                return Json(dados, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Medicamento/Create
        [Authorize(Roles = "Supervisor")]
        public ActionResult Create()
        {
            ViewBag.Recurso = "Tabelas > Medicamentos > Inclusão";
            return View();
        }

        // POST: Medicamento/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Supervisor")]
        public ActionResult Create([Bind(Include = "ID,NomeGenerico,Fabricante,NomeComercial")] Medicamento medicamento)
        {
            if (ModelState.IsValid)
            {
                Medicamento medicamentoExiste = db.Medicamento.Where(m => m.NomeComercial.Equals(medicamento.NomeComercial)).FirstOrDefault();

                if (medicamentoExiste == null)
                {
                    db.Medicamento.Add(medicamento);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("", "Já existe um Medicamento com este Nome comercial");
                }
            }

            return View(medicamento);
        }

        // GET: Medicamento/Edit/5
        [Authorize(Roles = "Supervisor")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medicamento medicamento = db.Medicamento.Find(id);
            if (medicamento == null)
            {
                return HttpNotFound();
            }

            ViewBag.Recurso = "Tabelas > Medicamentos > Alteração";
            return View(medicamento);
        }

        // POST: Medicamento/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Supervisor")]
        public ActionResult Edit([Bind(Include = "ID,NomeGenerico,Fabricante,NomeComercial")] Medicamento medicamento)
        {
            ConsultaMedicamento consulta = db.ConsultaMedicamento.Where(cm => cm.MedicamentoId == medicamento.ID).FirstOrDefault();

            if (consulta == null)
            {
                if (ModelState.IsValid)
                {
                    Medicamento medicamentoExiste = db.Medicamento.Where(m => m.NomeComercial.Equals(medicamento.NomeComercial) && m.ID != medicamento.ID).FirstOrDefault();

                    if (medicamentoExiste == null)
                    {
                        db.Entry(medicamento).State = EntityState.Modified;
                        db.SaveChanges();

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Já existe um Medicamento com este Nome comercial");
                    }
                }
            }
            else
            {
                ModelState.AddModelError("", "Este Medicamento está sendo utilizado e não pode ser alterado");
            }

            return View(medicamento);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeleteConfirmed(int id)
        {
            ConsultaMedicamento consulta = db.ConsultaMedicamento.Where(cm => cm.MedicamentoId == id).FirstOrDefault();

            if (consulta == null)
            {
                Medicamento med = db.Medicamento.Find(id);
                db.Medicamento.Remove(med);
                db.SaveChanges();
                return Json(new { success = true, responseText = "" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = false, responseText = "Este Medicamento está sendo utilizado e não pode ser excluído" }, JsonRequestBehavior.AllowGet);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
