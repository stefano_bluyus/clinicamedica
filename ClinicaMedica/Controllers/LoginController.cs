﻿using ClinicaMedica.Data;
using ClinicaMedica.DTO;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ClinicaMedica.Controllers
{
    public class LoginController : Controller
    {
        private Context db = new Context();

        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logar(LoginDTO funcionario)
        {
            if (ModelState.IsValid)
            {
                using (db)
                {
                    var obj = db.Funcionario.Where(f => f.Login.Equals(funcionario.Login) && f.Senha.Equals(funcionario.Senha)).FirstOrDefault();

                    if (obj != null)
                    {
                        FormsAuthentication.SetAuthCookie(funcionario.Login, false);

                        var authTicket = new FormsAuthenticationTicket(1, funcionario.Login, DateTime.Now, DateTime.Now.AddMinutes(5), false, obj.Perfil.ToString());
                        string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                        var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                        HttpContext.Response.Cookies.Add(authCookie);

                        Session["Login"] = obj.Id.ToString();

                        return RedirectToAction("Index", "Principal");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Usuário ou senha inválidos");
                        return View("Login");
                    }
                }
            }
            else
            {
                return View("Login");
            }
        }

       // [HttpPost]
       // [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Principal");
        }

        public ActionResult Logar()
        {
            return View("Login");
        }

        [Authorize(Roles = "Supervisor,Médico,Laboratório,Recepção")]
        public ActionResult TrocarSenha()
        {
            ViewBag.FuncionarioId = Session["Login"];
            return View("TrocarSenha");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TrocarSenha(int FuncionarioId, string Senha, string NovaSenha)
        {
            var obj = db.Funcionario.Where(f => f.Id.Equals(FuncionarioId) && f.Senha.Equals(Senha)).FirstOrDefault();

            if (obj != null)
            {
                obj.Senha = NovaSenha;

                db.Entry(obj).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Principal");
            }
            else
            {
                ViewBag.FuncionarioId = Session["Login"];
                ModelState.AddModelError("", "Senha inválida");
                return View("TrocarSenha");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}