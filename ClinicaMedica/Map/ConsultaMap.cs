﻿using ClinicaMedica.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClinicaMedica.Map
{ 
    public sealed class ConsultaMap : EntityTypeConfiguration<Consulta>
    {
        public ConsultaMap()
        {
            ToTable("Consultas");

            HasKey(x => x.Id)
                .Property(x => x.Id)
                .HasColumnName("id")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(c => c.PacienteId)
                .HasColumnName("PacienteId")
                .IsOptional();

            HasOptional(c => c.Paciente)
                .WithMany(c => c.Consultas)
                .HasForeignKey(c => c.PacienteId);


            Property(c => c.FuncionarioId)
                .HasColumnName("FuncionarioId")
                .IsRequired();

            HasRequired(c => c.Medico)
                .WithMany(c => c.Consultas)
                .HasForeignKey(c => c.FuncionarioId);


        }
    }
}