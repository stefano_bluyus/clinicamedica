﻿using ClinicaMedica.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClinicaMedica.Map
{
    public sealed class CidMap : EntityTypeConfiguration<Cid>
    {
        public CidMap()
        {
            ToTable("Cids");

            HasKey(c => c.ID)
                .Property(c => c.ID)
                .HasColumnName("Id")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(c => c.Codigo)
            .IsRequired()
            .HasColumnType("varchar")
            .HasMaxLength(6);

            Property(c => c.Descricao)
            .IsRequired()
            .HasColumnType("varchar")
            .HasMaxLength(255);

        }
    }
}