﻿using ClinicaMedica.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ClinicaMedica.Map
{
    public sealed class ConsultaMedicamentoMap : EntityTypeConfiguration<ConsultaMedicamento>
    {
        public ConsultaMedicamentoMap()
        {
            ToTable("ConsultasMedicamentos");

            HasKey(cm => new { cm.ConsultaId, cm.MedicamentoId });

            Property(cm => cm.ConsultaId)
                .IsRequired()
                .HasColumnName("ConsultaId");

            Property(cm => cm.MedicamentoId)
                .IsRequired()
                .HasColumnName("MedicamentoId");

            Property(cm => cm.Posologia)
                .IsRequired()
                .HasColumnName("Posologia")
                .HasColumnType("varchar")
                .HasMaxLength(25);

            HasRequired(cm => cm.Consulta)
                .WithMany(cm => cm.ConsultaMedicamentos)
                .HasForeignKey(cm => cm.ConsultaId)
                .WillCascadeOnDelete(false);

            HasRequired(cm => cm.Medicamento)
                .WithMany(cm => cm.ConsultaMedicamentos)
                .HasForeignKey(cm => cm.MedicamentoId)
                .WillCascadeOnDelete(false);
        }
    }
}