﻿using ClinicaMedica.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ClinicaMedica.Map
{
    public sealed class ConsultaExameMap : EntityTypeConfiguration<ConsultaExame>
    {
        public ConsultaExameMap()
        {
            ToTable("ConsultasExames");

            HasKey(ce => new { ce.ConsultaId, ce.ExameId });

            Property(ce => ce.ConsultaId)
                .IsRequired()
                .HasColumnName("ConsultaId");

            Property(ce => ce.ExameId)
                .IsRequired()
                .HasColumnName("ExameId");

            Property(ce => ce.Realizado)
                .IsRequired()
                .HasColumnName("Realizado");

            Property(cm => cm.Login)
             .IsRequired()
             .HasColumnType("varchar")
             .HasMaxLength(10);

            Property(cm => cm.Senha)
             .IsRequired()
             .HasColumnType("varchar")
             .HasMaxLength(10);

            HasRequired(cm => cm.Consulta)
                .WithMany(cm => cm.ConsultaExames)
                .HasForeignKey(cm => cm.ConsultaId)
                .WillCascadeOnDelete(false);

            HasRequired(cm => cm.Exame)
                .WithMany(cm => cm.ConsultaExames)
                .HasForeignKey(cm => cm.ExameId)
                .WillCascadeOnDelete(false);
        }
    }
}