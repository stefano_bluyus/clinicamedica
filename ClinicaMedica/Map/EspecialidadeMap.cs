﻿using ClinicaMedica.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ClinicaMedica.Map
{
    public sealed class EspecialidadeMap : EntityTypeConfiguration<Especialidade>
    {
        public EspecialidadeMap()
        {
            ToTable("Especialidades");

            HasKey(e => e.ID)
                .Property(e => e.ID)
                .HasColumnName("ID")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(e => e.Descricao)
            .IsRequired()
            .HasColumnType("varchar")
            .HasMaxLength(50);
        }
    }
}