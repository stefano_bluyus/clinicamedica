﻿using ClinicaMedica.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ClinicaMedica.Map
{
    public sealed class ConsultaCidMap : EntityTypeConfiguration<ConsultaCid>
    {
        public ConsultaCidMap()
        {
            ToTable("ConsultasCids");

            HasKey(ce => new { ce.ConsultaId, ce.CidId });

            Property(ce => ce.ConsultaId)
                .IsRequired()
                .HasColumnName("ConsultaId");

            Property(ce => ce.CidId)
                .IsRequired()
                .HasColumnName("CidId");

            HasRequired(cm => cm.Consulta)
                .WithMany(cm => cm.ConsultaCids)
                .HasForeignKey(cm => cm.ConsultaId)
                .WillCascadeOnDelete(false);

            HasRequired(cm => cm.Cid)
                .WithMany(cm => cm.ConsultaCids)
                .HasForeignKey(cm => cm.CidId)
                .WillCascadeOnDelete(false);
        }
    }
}