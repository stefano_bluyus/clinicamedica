﻿using ClinicaMedica.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClinicaMedica.Map
{
    public sealed class PacienteMap : EntityTypeConfiguration<Paciente>
    {
        public PacienteMap()
        {
            ToTable("Pacientes");

            HasKey(p => p.Id)
                .Property(p => p.Id)
                .HasColumnName("Id")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasMany(p => p.Consultas)
                .WithOptional(p => p.Paciente)
                .HasForeignKey(p => p.PacienteId);

            Property(p => p.Nome)
            .IsRequired()
            .HasColumnType("varchar")
            .HasMaxLength(130);

            Property(p => p.NomeMae)
            .HasColumnType("varchar")
            .HasMaxLength(130);

            Property(p => p.CPF)
           .HasColumnType("varchar")
           .HasMaxLength(14);

            Property(p => p.Telefone)
           .HasColumnType("varchar")
           .HasMaxLength(20);

        }
    }
}