﻿using ClinicaMedica.Models;
using System.Data.Entity.ModelConfiguration;

namespace ClinicaMedica.Map
{
    public sealed class AgendaMap : EntityTypeConfiguration<Agenda>
    {
        public AgendaMap()
        {
            ToTable("Agendas");

            HasKey(x => x.Id)
                .Property(x => x.Id)
                .HasColumnName("id")
                .IsRequired();

            HasRequired(x => x.Funcionario)
                        .WithOptional(x => x.Agenda);


            Property(a => a.EspecialidadeId)
            .HasColumnName("EspecialidadeId")
            .IsRequired();

            Property(a => a.CRM)
            .IsRequired()
            .HasColumnType("varchar")
            .HasMaxLength(15);


        }
    }
}