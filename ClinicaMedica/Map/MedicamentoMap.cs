﻿using ClinicaMedica.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ClinicaMedica.Map
{
    public sealed class MedicamentoMap : EntityTypeConfiguration<Medicamento>
    {
        public MedicamentoMap()
        {
            ToTable("Medicamentos");

            HasKey(c => c.ID)
                .Property(c => c.ID)
                .HasColumnName("Id")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(m => m.Fabricante)
            .IsRequired()
            .HasColumnType("varchar")
            .HasMaxLength(25);

            Property(m => m.NomeComercial)
            .IsRequired()
            .HasColumnType("varchar")
            .HasMaxLength(50);

            Property(m => m.NomeGenerico)
            .IsRequired()
            .HasColumnType("varchar")
            .HasMaxLength(100);
        }
    }
}