﻿using ClinicaMedica.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ClinicaMedica.Map
{
    public sealed class FuncionarioMap : EntityTypeConfiguration<Funcionario>
    {
        public FuncionarioMap()
        {
            ToTable("Funcionarios");

            HasKey(x => x.Id)
                .Property(x => x.Id)
                .HasColumnName("Id")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasOptional(x => x.Agenda)
                .WithRequired(x => x.Funcionario);


            HasMany(f => f.Consultas)
                .WithRequired(f => f.Medico)
                .HasForeignKey(f => f.FuncionarioId);

            Property(f => f.Nome)
            .IsRequired()
            .HasColumnType("varchar")
            .HasMaxLength(130);

            Property(f => f.Login)
            .IsRequired()
            .HasColumnType("varchar")
            .HasMaxLength(10);

            Property(f => f.Senha)
            .IsRequired()
            .HasColumnType("varchar")
            .HasMaxLength(10);
        }
    }
}