﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClinicaMedica.Models
{
    public class ConsultaExame
    {
        public int ConsultaId { get; set; }
        public virtual Consulta Consulta { get; set; }

        public int ExameId { get; set; }
        public virtual Exame Exame { get; set; }

        public Boolean Realizado { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NumeroPedido { get; set; }

        public string Login { get; set; }

        public string Senha { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DataPedido { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DataLiberacao { get; set; }
    }
}