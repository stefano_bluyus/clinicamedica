﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClinicaMedica.Models
{
    public class Medicamento
    {
        public Medicamento()
        {
            ConsultaMedicamentos = new HashSet<ConsultaMedicamento>();
        }

        public int ID { get; set; }

        [Required(ErrorMessage = "O Nome genérico é obrigatório", AllowEmptyStrings = false)]
        [StringLength(250, MinimumLength = 1)]
        [Display(Name = "Nome genérico")]
        public string NomeGenerico { get; set; }

        [Required(ErrorMessage = "O Fabricante é obrigatório", AllowEmptyStrings = false)]
        [StringLength(250, MinimumLength = 1)]
        [Display(Name = "Fabricante")]
        public string Fabricante { get; set; }

        [Required(ErrorMessage = "O Nome comercial é obrigatório", AllowEmptyStrings = false)]
        [StringLength(250, MinimumLength = 1)]
        [Display(Name = "Nome comercial")]
        public string NomeComercial { get; set; }

        public virtual ICollection<ConsultaMedicamento> ConsultaMedicamentos { get; set; }
    }
}