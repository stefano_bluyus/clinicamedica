﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicaMedica.Models
{
    public class Consulta
    {
        public Consulta()
        { 
            ConsultaCids = new HashSet<ConsultaCid>();
            ConsultaMedicamentos = new HashSet<ConsultaMedicamento>();
            ConsultaExames = new HashSet<ConsultaExame>();
        }

        public int Id { get; set; }

        public DateTime DataHora { get; set; }

        public string Queixas { get; set; }

        public int? PacienteId { get; set; }
        public virtual Paciente Paciente { get; set; }

        public int FuncionarioId { get; set; }
        public virtual Funcionario Medico { get; set; }

        public virtual ICollection<ConsultaCid> ConsultaCids { get; set; }

        public virtual ICollection<ConsultaMedicamento> ConsultaMedicamentos { get; set; }

        public virtual ICollection<ConsultaExame> ConsultaExames { get; set; }
    }
}