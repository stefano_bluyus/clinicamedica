﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ClinicaMedica.Models
{
    public class Agenda
    {
        [Key]
        public int Id { get; set; }

        public virtual Funcionario Funcionario { get; set; }

        public string CRM { get; set; }

        [Display(Name = "Especialidade")]
        public int EspecialidadeId { get; set; }
        public virtual Especialidade Especialidade { get; set; }

        public bool Segunda { get; set; }
        [Display(Name = "Terça")]
        public bool Terca { get; set; }
        public bool Quarta { get; set; }
        public bool Quinta { get; set; }
        public bool Sexta { get; set; }
        [Display(Name = "Sábado")]
        public bool Sabado { get; set; }
        public bool Domingo { get; set; }

        [Display(Name = "Quantidade de conultas no dia")]
        [RangeAttribute(1, 40)]
        public int QuantidadeConsultas { get; set; }

        [Display(Name = "Tempo da consulta (em minutos)")]        
        [RangeAttribute(15, 120)]
        public int TempoConsulta { get; set; }

        [Display(Name = "Horário de início")]
        [DataType(DataType.Time)]
        public DateTime HorarioInicio { get; set; }
    }
}