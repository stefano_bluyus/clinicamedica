﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ClinicaMedica.Models
{

    public class Funcionario
    {
        public Funcionario()
        {
            Consultas = new HashSet<Consulta>();
        }

        public int Id { get; set; }

        [Required(ErrorMessage = "O Nome é obrigatório", AllowEmptyStrings = false)]
        [StringLength(130, MinimumLength = 3)]
        public string Nome { get; set; }

        [Required(ErrorMessage = "O Login é obrigatório", AllowEmptyStrings = false)]
        [StringLength(10, MinimumLength = 3)]
        public string Login { get; set; }

        [Required(ErrorMessage = "A senha é obrigatória")]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        public virtual Agenda Agenda { get; set; }

        [DisplayName("Perfil")]
        [Required(ErrorMessage = "O Perfil de acesso é obrigatório")]
        public TipoAcesso Perfil { get; set; }

        public virtual ICollection<Consulta> Consultas { get; set; }
    }

    public enum TipoAcesso
    {
        Médico = 1,
        Recepção = 2,
        Laboratório = 3,
        Supervisor = 4
    }
}