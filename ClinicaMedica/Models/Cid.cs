﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClinicaMedica.Models
{
    public class Cid
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "O Código é obrigatório", AllowEmptyStrings = false)]
        [StringLength(6, MinimumLength = 1)]
        [Display(Name = "Código")]
        public string Codigo { get; set; }

        [Required(ErrorMessage = "A Descrição é obrigatória", AllowEmptyStrings = false)]
        [Display(Name = "Descrição")]
        [StringLength(250, MinimumLength = 1)]
        public string Descricao { get; set; }

        public virtual ICollection<ConsultaCid> ConsultaCids { get; set; }

    }
}