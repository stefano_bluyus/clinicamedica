﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ClinicaMedica.Models
{
    public class Paciente
    {
        public Paciente()
        {
            Consultas = new HashSet<Consulta>();
        }

        private string cpf { get; set; }

        public int Id { get; set; }

        [Required(ErrorMessage = "O Nome é obrigatório", AllowEmptyStrings = false)]
        [StringLength(130, MinimumLength = 3)]
        public string Nome { get; set; }

        [Required(ErrorMessage = "O Nome da mãe é obrigatório", AllowEmptyStrings = false)]
        [StringLength(130, MinimumLength = 3)]
        [Display(Name = "Nome da mãe")]
        public string NomeMae { get; set; }

        [Required(ErrorMessage = "A Data de nascimento é obrigatória")]
        [Display(Name = "Data de nascimento")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date, ErrorMessage = "Data em formato inválido")]
        public DateTime DataNascimento { get; set; }

        [StringLength(14, MinimumLength = 11)]
        public string CPF {
            get
            {
                string teste = cpf;

                if ((cpf != null) && (cpf != "") && (teste.Length < 14))
                {
                    teste = Convert.ToUInt64(cpf).ToString(@"000\.000\.000\-00");
                }

                return teste;
            }
            set { cpf = value; }
        }

        [DataType(DataType.PhoneNumber)]
        [StringLength(20)]
        public string Telefone { get; set; }

        public virtual ICollection<Consulta> Consultas { get; set; }

    }
}