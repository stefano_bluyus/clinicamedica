﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClinicaMedica.Models
{
    public class Especialidade
    {
        public Especialidade()
        {
            Agenda = new HashSet<Agenda>();
        }

        public int ID { get; set; }

        [Required(ErrorMessage = "A Descrição da Especialidade é obrigatória", AllowEmptyStrings = false)]
        [StringLength(50, MinimumLength = 7)]
        [Display(Name = "Descrição")]
        public string Descricao { get; set; }

        public virtual ICollection<Agenda> Agenda { get; set; }
    }
}