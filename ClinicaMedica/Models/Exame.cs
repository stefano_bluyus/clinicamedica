﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClinicaMedica.Models
{
    public class Exame
    {
        public Exame()
        {
            ConsultaExames = new HashSet<ConsultaExame>();
        }

        public int ID { get; set; }

        [Required(ErrorMessage = "O código do Exame é obrigatório", AllowEmptyStrings = false)]
        [StringLength(10, MinimumLength = 3)]
        [Display(Name = "Código")]
        public string Codigo { get; set; }

        [Required(ErrorMessage = "A Descrição do Exame é obrigatória", AllowEmptyStrings = false)]
        [StringLength(250, MinimumLength = 1)]
        [Display(Name = "Descrição")]
        public string Descricao { get; set; }

        public virtual ICollection<ConsultaExame> ConsultaExames { get; set; }
    }
}