﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicaMedica.Models
{
    public class ConsultaMedicamento
    {
        public int ConsultaId { get; set; }
        public virtual Consulta Consulta { get; set; }

        public int MedicamentoId { get; set; }
        public virtual Medicamento Medicamento { get; set; }

        public string Posologia { get; set; }
    }
}