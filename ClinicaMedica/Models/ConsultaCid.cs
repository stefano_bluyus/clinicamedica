﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicaMedica.Models
{
    public class ConsultaCid
    {
        public int ConsultaId { get; set; }
        public virtual Consulta Consulta { get; set; }

        public int CidId { get; set; }
        public virtual Cid Cid { get; set; }
    }
}