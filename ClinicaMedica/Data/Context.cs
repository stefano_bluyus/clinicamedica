﻿using ClinicaMedica.Map;
using ClinicaMedica.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ClinicaMedica.Data
{
    public class Context: DbContext
    {
        public Context() : base("ConexaoClinica") {

            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;

            Database.SetInitializer<Context>(new UniDBInitializer<Context>());
        }


        public DbSet<Cid> Cid { get; set; }
        public DbSet<Exame> Exame { get; set; }
        public DbSet<Especialidade> Especialidade { get; set; }
        public DbSet<Agenda> Agenda { get; set; }
        public DbSet<Funcionario> Funcionario { get; set; }
        public DbSet<Paciente> Paciente { get; set; }
        public DbSet<Medicamento> Medicamento { get; set; }
        public DbSet<ConsultaMedicamento> ConsultaMedicamento { get; set; }
        public DbSet<ConsultaExame> ConsultaExame { get; set; }
        public DbSet<ConsultaCid> ConsultaCid { get; set; }
        public DbSet<Consulta> Consulta { get; set; }

        private class UniDBInitializer<T> : DropCreateDatabaseIfModelChanges<Context>
        {

            protected override void Seed(Context context)
            {
                IList<Especialidade> especialidades = new List<Especialidade>();

                especialidades.Add(new Especialidade()
                {
                    Descricao = "Ortopedia"
                });

                especialidades.Add(new Especialidade()
                {
                    Descricao = "Dermatologia"
                });

                foreach (Especialidade especialidade in especialidades)
                    context.Especialidade.Add(especialidade);


                IList<Funcionario> funcs = new List<Funcionario>();

                funcs.Add(new Funcionario()
                {
                    Login = "supervisor",
                    Nome = "supervisor",
                    Senha = "123456",
                    Perfil = TipoAcesso.Supervisor
                });

                funcs.Add(new Funcionario()
                {
                    Login = "lab",
                    Nome = "lab",
                    Senha = "123456",
                    Perfil = TipoAcesso.Laboratório
                });

                funcs.Add(new Funcionario()
                {
                    Login = "rec",
                    Nome = "rec",
                    Senha = "123456",
                    Perfil = TipoAcesso.Recepção
                });

                funcs.Add(new Funcionario()
                {
                    Login = "medico",
                    Nome = "medico",
                    Senha = "123456",
                    Perfil = TipoAcesso.Médico
                });

                foreach (Funcionario funcionario in funcs)
                    context.Funcionario.Add(funcionario);

                IList<Cid> cids = new List<Cid>();

                cids.Add(new Cid()
                {
                    Codigo = "a90",
                    Descricao = "Dengue"
                });

                cids.Add(new Cid()
                {
                    Codigo = "A00.0",
                    Descricao = "Cólera devida a Vibrio cholerae 01, biótipo cholerae"
                });

                cids.Add(new Cid()
                {
                    Codigo = "A01.0",
                    Descricao = "Febre tifóide"
                });

                foreach (Cid cid in cids)
                    context.Cid.Add(cid);

                IList<Exame> exames = new List<Exame>();

                exames.Add(new Exame()
                {
                    Codigo = "01-01-001",
                    Descricao = "Aplicação de hipossensibilizante em consultório(AHC), exclusive o alé"
                });

                exames.Add(new Exame()
                {
                    Codigo = "01-04-011",
                    Descricao = "Investigação ultra-sônica com teste de stress em esteira e com registro"
                });

                exames.Add(new Exame()
                {
                    Codigo = "01-07-010",
                    Descricao = "Eletroencefalograma digital +mapeamento cerebral(EEG / MC)"
                });

                foreach (Exame exame in exames)
                    context.Exame.Add(exame);          

                IList<Medicamento> medicamentos = new List<Medicamento>();

                medicamentos.Add(new Medicamento()
                {
                    Fabricante = "Bayer",
                    NomeComercial = "Aspirina",
                    NomeGenerico = "Ácido acetilsalicílico"
                });

                medicamentos.Add(new Medicamento()
                {
                    Fabricante = "Sanofi-Aventis",
                    NomeComercial = "AAS Protect",
                    NomeGenerico = "Ácido acetilsalicílico"
                });


                foreach (Medicamento medicamento in medicamentos)
                    context.Medicamento.Add(medicamento);

                IList<Paciente> pacientes = new List<Paciente>();

                pacientes.Add(new Paciente()
                {
                    Nome = "João",
                    NomeMae = "Ana",
                    DataNascimento = new System.DateTime(1980, 01, 01),
                    CPF = "11111111111",
                    Telefone = "12988457896"                    
                });

                pacientes.Add(new Paciente()
                {
                    Nome = "Maria",
                    NomeMae = "Júlia",
                    DataNascimento = new System.DateTime(1990, 02, 01),
                    CPF = "11111111111",
                    Telefone = "12988457896"
                });


                foreach (Paciente paciente in pacientes)
                    context.Paciente.Add(paciente);


                base.Seed(context);
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Conventions.Remove<PluralizingEntitySetNameConvention>();

            modelBuilder.Configurations.Add(new FuncionarioMap());
            modelBuilder.Configurations.Add(new AgendaMap());
            modelBuilder.Configurations.Add(new PacienteMap());
            modelBuilder.Configurations.Add(new CidMap());
            modelBuilder.Configurations.Add(new MedicamentoMap());
            modelBuilder.Configurations.Add(new ConsultaMap());
            modelBuilder.Configurations.Add(new EspecialidadeMap());
            modelBuilder.Configurations.Add(new ConsultaMedicamentoMap());
            modelBuilder.Configurations.Add(new ConsultaExameMap());
            modelBuilder.Configurations.Add(new ConsultaCidMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}