﻿function RemoverValidacao(campo) {
    if ($('#msgval' + campo).length) {
        $('#msgval' + campo).remove();
    }
}

function cpf_mask(v) {

    if (v.length > 0) {
        v = v.replace(/\D/g, "") //Remove tudo o que não é dígito
        v = v.slice(0, 3) + '.' + v.slice(3, 6) + '.' + v.slice(6, 9) + '-' + v.slice(9, 11);
    }
    return v
}