﻿$(document).ready(function () {
    $("#Fabricante").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Medicamento/GetFabricante",
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.Fabricante, value: item.Fabricante };
                    }))

                }
            })
        },

        messages: {
            // noResults: "", results: ""
        }
    });
})