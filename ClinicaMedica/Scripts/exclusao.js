﻿

function Excluir(controller, id)
{
    var token = $('input[name="__RequestVerificationToken"]').val();

    $.ajax({
        url: "/" + controller + "/DeleteConfirmed",
        type: "POST",
        dataType: "json",
        data: { __RequestVerificationToken: token, id: id },
        success: function (response) {
            if (response.success) {
                $('#id' + id).remove();
                
            } else {
                alert(response.responseText);
            }
        },
    });
}