﻿function ExcluirCid(cid, consulta) {

    $.ajax({
        url: "/Consulta/DeleteCid",
        type: "POST",
        dataType: "json",
        data: { CidId: cid, ConsultaId: consulta },
        success: function (data) {
            $('#c' + cid + '_' + consulta).remove();
        },
    });
}

function AddCid(consulta, cid, codigocid) {

    $.ajax({
        url: "/Consulta/AddCid",
        type: "POST",
        dataType: "json",
        data: { CidId: cid, ConsultaId: consulta },
        success: function (data) {
            $("#cids_table").append('<tr id=c' + cid + '_' + consulta + '>' +
           // $("#cids_table > tbody").append('<tr id=c' + cid + '_' + consulta + '>' +
                '<td>' + codigocid + '</td>' +
                '<td> <a href="#" onclick="ExcluirCid(' + cid + ',' + consulta + ')">Excluir</a></td>' +
                "</tr> ");

            $("#nomecid").val("");
        },
    });
}


function AddMedicamento(consulta)
{
    RemoverValidacao("dosemedicamento");
    RemoverValidacaoNomeMedicamento();

    dose = $("#dosemedicamento").val();
    id = $('#idmedicamento').val();
    nome = $('#nomecomercial').val();

    validado = true;

    if (id == "") {
        $('#valnomemedicamento').append('<span for="nomemedicamento" class="" id="msgvalnomemedicamento">É necessário selecionar um Medicamento</span>');
        validado = false;
    }

    if (dose == "")
    {
        $('#valdosemedicamento').append('<span for="dosemedicamento" class="" id="msgvaldosemedicamento">É necessário informar a Posologia</span>');
        validado = false;
    }

    if (validado)
    {
        $.ajax({
            url: "/Consulta/AddMedicamento",
            type: "POST",
            dataType: "json",

            data: {MedicamentoId: id,
                   Posologia: dose,
                   ConsultaId: consulta},

            success: function (data) {
                $("#medicamento_table").append('<tr id=m' + id + '_' + consulta + '>' +
                    // $("#cids_table > tbody").append('<tr id=c' + cid + '_' + consulta + '>' +
                    '<td>' + nome + '</td>' +
                    '<td>' + dose + '</td>' +
                    '<td> <a href="#" onclick="ExcluirMedicamento(' + id + ',' + consulta + ')">Excluir</a></td>' +
                    "</tr> ");

                $("#nomemedicamento").val("");
                $("#dosemedicamento").val("");
                $("#idmedicamento").val("");
            },
        });

    }
}

function ExcluirMedicamento(medicamento, consulta) {

    $.ajax({
        url: "/Consulta/DeleteMedicamento",
        type: "POST",
        dataType: "json",
        data: { MedicamentoId: medicamento, ConsultaId: consulta },
        success: function (data) {
            $('#m' + medicamento + '_' + consulta).remove();
        },
    });
}

function ExcluirExame(exame, consulta) {

    $.ajax({
        url: "/Consulta/DeleteExame",
        type: "POST",
        dataType: "json",
        data: { ExameId: exame, ConsultaId: consulta },
        success: function (response) {
            if (response.success) {
                console.log(response.success)
                $('#e' + exame + '_' + consulta).remove();

            } else {
                alert(response.responseText);
            }
        },
    });
}

function AddExame(consulta, exame, codigoexame) {

    $.ajax({
        url: "/Consulta/AddExame",
        type: "POST",
        dataType: "json",
        data: { ExameId: exame, ConsultaId: consulta },
        success: function (data) {
            $("#exames_table").append('<tr id=e' + exame + '_' + consulta + '>' +
                '<td>' + codigoexame + '</td>' +
                '<td> <a href="#" onclick="ExcluirExame(' + exame + ',' + consulta + ')">Excluir</a></td>' +
                "</tr> ");

            $("#nomeexame").val("");
        },
    });
}

function RemoverValidacao(campo) {
    if ($('#msgval' + campo).length) {
        $('#msgval' + campo).remove();
    }
}

function RemoverValidacaoNomeMedicamento() {
    if ($('#msgvalvalnomemedicamento').length) {
        $('#msgvalvalnomemedicamento').remove();
    }
}

function Teste(label, name, value) {
    $('#idmedicamento').attr('value', name);
    $('#nomecomercial').attr('value', value);
    $('#nomemedicamento').attr('value', label);
    $('#nomemedicamento').val(label);
}