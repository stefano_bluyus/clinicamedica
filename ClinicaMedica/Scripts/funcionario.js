﻿$(document).ready(function () { perfil_change() });


function perfil_change(e) {
    if ($('#cmbPerfil').val() == 1) {
        $("#agenda_crm").show();
    }
    else {
        $("#agenda_crm").hide();
    }

    RemoverValidacao('perfil');
}

function ValidarCampos()
{
    RemoverValidacao('crm');
    RemoverValidacao('dias');
    RemoverValidacao('perfil');

    if ($('#msgerro').length) {
        $('#msgerro').remove();
    }   

    perfil = $('#cmbPerfil').val();

    if (perfil == 1)
    {
        validado = true;
        crm = $('#crm').val();

        seg = $('#ckbSeg').is(':checked');
        ter = $('#ckbTer').is(':checked');
        qua = $('#ckbQua').is(':checked');
        qui = $('#ckbQui').is(':checked');
        sex = $('#ckbSex').is(':checked');
        sab = $('#ckbSab').is(':checked');
        dom = $('#ckbDom').is(':checked');

        if (crm == "") {
            $('#valcrm').append('<span for="CRM" class="" id="msgvalcrm">O CRM é obrigatório</span>');
            validado = false;
        }

        if ((seg == false) && (ter == false) && (qua == false) && (qui == false) && (sex == false) && (sab == false) && (dom == false))
        {
            $('#valdias').append('<span for="Domingo" class="" id="msgvaldias">É necessário escolher ao menos um dia de atendimento</span>');
            validado = false;
        }
    }
    else
    {
        validado = true;

        if (perfil == 0)
        {
            $('#valperfil').append('<span for="Perfil" class="" id="msgvalperfil">O Perfil de acesso é obrigatório</span>');
            validado = false;
        }
    }

    if (validado != false) {
        $('#frmFunc').submit();
    }
}

function RemoverValidacao(campo) {
    if ($('#msgval' + campo).length) {
        $('#msgval' + campo).remove();
    }
}