﻿

function ExcluirAgenda(data, medico) {

    var token = $('input[name="__RequestVerificationToken"]').val();

    $.ajax({
        url: "/Agenda/ExcluirAgenda",
        type: "POST",
        dataType: "json",
        data: { __RequestVerificationToken: token, data: data, medico: medico },
        success: function (response) {
            if (response.success) {
                $('#id' + medico).remove();
                
            } else {
                // DoSomethingElse()
                alert(response.responseText);
            }
        },
    });
}