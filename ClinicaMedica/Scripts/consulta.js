﻿function ToJavaScriptDate(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    return ("00" + dt.getDate()).slice(-2) + '/' + ("00" + (dt.getMonth() + 1)).slice(-2) + "/" + dt.getFullYear() + ' ' + ("00" + dt.getHours()).slice(-2) + ':' + ("00" + dt.getMinutes()).slice(-2);
}

function ToJavaScriptOnlyDate(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    return ("00" + dt.getDate()).slice(-2) + '/' + ("00" + (dt.getMonth() + 1)).slice(-2) + "/" + dt.getFullYear();
}

function cpf_mask(v) {

    if ((v != null) && (v.length > 0))
    {
        v = v.replace(/\D/g, "") //Remove tudo o que não é dígito
        v = v.slice(0, 3) + '.' + v.slice(3, 6) + '.' + v.slice(6, 9) + '-'+ v.slice(9, 11);
    }  
    return v
}

function buscarPacientes(operacao)
{
    var Nome = $("#SearchString").val();
    var token = $('input[name="__RequestVerificationToken"]').val();

    $.ajax(
        {
            type: 'POST',
            url: '/Marcacao/BuscarPacientes',
            data: { __RequestVerificationToken: token, Nome: Nome, Operacao: operacao },
            success: function (response) {

                var trHTML = '';

                if (response.length > 0) {

                    trHTML += '<thead><tr>' +
                        ' <th>Nome</th>' +
                        ' <th>Nome da mãe</th>' +
                        ' <th>Data de nascimento</th>' +
                        ' <th>CPF</th>' +
                        ' <th></th>' +
                        '</tr></thead>';
                }
                else {
                    trHTML = '<span id="msgvaltel">Não existem pacientes com este nome</span>';
                }


                for (var f = 0; f < response.length; f++) {
                    trHTML += '<tr>' +
                        '<td>' + response[f]['Nome'] + '</td>' +
                        '<td>' + response[f]['NomeMae'] + '</td>' +
                        '<td>' + ToJavaScriptOnlyDate(response[f]['DataNascimento']) + '</td>' +
                        '<td>' + cpf_mask(response[f]['CPF']) + '</td>' +
                        '<td><a href="#" onclick="Marcar(' + response[f]['Id'] + ')">Selecionar</a></td >' +
                        '</tr>';
                }

                $('#pacientes_table').html(trHTML);
            }
        });
}


function Marcar(id) {
    var input = $("<input>")
        .attr("type", "hidden")
        .attr("name", "Id").val(id);
    $('form:first').append($(input));

    $('form:first').submit();

    return 1;
}

function MarcarConsulta(id) {
    var input = $("<input>")
        .attr("type", "hidden")
        .attr("name", "ConsultaId").val(id);
    $('form:first').append($(input));

    var input = $("<input>")
        .attr("type", "hidden")
        .attr("name", "Operacao").val("M");
    $('form:first').append($(input));


    $('form:first').submit();

    return 1;
}

function DesmarcarConsulta(id) {
    var input = $("<input>")
        .attr("type", "hidden")
        .attr("name", "ConsultaId").val(id);
    $('form:first').append($(input));

    var input = $("<input>")
        .attr("type", "hidden")
        .attr("name", "Operacao").val("D");
    $('form:first').append($(input));


    $('form:first').submit();

    return 1;
}

function RemoverValidacao(campo) {
    if ($('#msgval' + campo).length) {
        $('#msgval' + campo).remove();
    }
}

function buscarConsultas(operacao)
{
    var token = $('input[name="__RequestVerificationToken"]').val();

    if ($('#msgerro').length) {
        $('#msgerro').remove();
    }    

    RemoverValidacao('dataconsulta');
    RemoverValidacao('especialidade');

    var dataConsulta = $("#dataconsulta").val();
    var especialidadeId = $("#especialidade option:selected").val();
    var paciente = $("#PacienteId").val();

    validado = true;

    if (dataConsulta == "")
    {
        $('#valdataconsulta').append('<span for="data" class="" id="msgvaldataconsulta">A data da consulta é obrigatória.</span>');
        validado = false;
    }

    if (especialidadeId == "")
    {
        $('#valespecialidade').append('<span for="especialidade" class="" id="msgvalespecialidade">Selecione uma especialidade.</span>');
        validado = false;
    }

    if (validado == true)
    {
        $.ajax(
            {
                type: 'POST',
                url: '/Marcacao/BuscarConsultas',
                data: { __RequestVerificationToken: token, dataConsulta: dataConsulta, EspecialidadeId: especialidadeId, Operacao: operacao, PacienteId: paciente},
                success: function (response) {

                    console.log(response.length);
                    var trHTML = '';

                    if (response.length > 0) {

                        trHTML += '<thead><tr>' +
                            ' <th>Data</th>' +
                            ' <th>Médico</th>' +
                            ' <th></th>' +
                            '</tr></thead>';
                    }
                    else {
                        trHTML = '<p>Não existem consultas com esses parâmetros</p>';
                    }

                    if (operacao == "M") {
                        for (var f = 0; f < response.length; f++) {
                            trHTML += '<tr>' +
                                '<td>' + ToJavaScriptDate(response[f]['Data']) + '</td>' +
                                '<td>' + response[f]['NomeMedico'] + '</td>' +
                                '<td><a href="#" onclick="MarcarConsulta(' + response[f]['Id'] + ')">Marcar</a></td >' +
                                '</tr>';
                        }
                    }
                    else {
                        for (var f = 0; f < response.length; f++) {
                            trHTML += '<tr>' +
                                '<td>' + ToJavaScriptDate(response[f]['Data']) + '</td>' +
                                '<td>' + response[f]['NomeMedico'] + '</td>' +
                                '<td><a href="#" onclick="DesmarcarConsulta(' + response[f]['Id'] + ')">Desmarcar</a></td >' +
                                '</tr>';
                        }
                    }                    

                    $('#records_table').html(trHTML);
                    console.log(trHTML);
                }
            });
    }
}