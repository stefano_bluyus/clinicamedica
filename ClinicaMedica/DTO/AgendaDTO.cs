﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ClinicaMedica.DTO
{
    public class AgendaDTO
    {
        [Required]
        [Display(Name = "Início")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date, ErrorMessage = "Data em formato inválido")]
        public DateTime DataInicio { get; set; }

        [Required]
        [Display(Name = "Término")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date, ErrorMessage = "Data em formato inválido")]
        public DateTime DataTermino { get; set; }
    }
}