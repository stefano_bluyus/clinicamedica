﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ClinicaMedica.DTO
{
    public class ConsultasMedicoDTO
    {
        public int IdConsulta { get; set; }

        public string NomePaciente { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date)]
        public DateTime Horario { get; set; }
    }    
}