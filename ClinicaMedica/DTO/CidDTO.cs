﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicaMedica.DTO
{
    public class TabelaDTO
    {
        public int ID { get; set; }
        public string Codigo { get; set; }
        public string Descricao { get; set; }
    }
}