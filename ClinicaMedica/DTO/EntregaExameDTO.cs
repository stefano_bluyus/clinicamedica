﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ClinicaMedica.DTO
{
    public class EntregaExameDTO
    {
        [Required]
        [Display(Name = "Número do pedido")]
        public int NumeroPedido { get; set; }

        [Required]
        [Display(Name = "Arquivo")]
        public HttpPostedFileBase File { get; set; }

        [Required]
        [Display(Name = "Sobrescrever exame entregue?")]
        public bool Sobrescrever { get; set; }

    }
}