﻿using System.ComponentModel.DataAnnotations;

namespace ClinicaMedica.DTO
{
    public class TrocarSenhaDTO
    {
        [Required(ErrorMessage = "Informe a Senha")]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        [Display(Name ="Nova senha")]
        [Required(ErrorMessage = "Informe a nova Senha")]
        [DataType(DataType.Password)]
        public string NovaSenha { get; set; }

        public string FuncionarioId { get; set; }
    }
}