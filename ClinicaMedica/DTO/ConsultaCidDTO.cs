﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicaMedica.DTO
{
    public class ConsultaCidDTO
    {
        public int Id { get; set; }
        public string Cid { get; set; }
    }
}