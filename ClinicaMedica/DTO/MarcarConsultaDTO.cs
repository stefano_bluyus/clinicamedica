﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ClinicaMedica.DTO
{
    public class MarcarConsultaDTO
    {
        [Required(ErrorMessage = "A data da consulta é obrigatória.", AllowEmptyStrings = false)]
        [Display(Name = "Data da consulta (a partir de)")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date, ErrorMessage = "Data em formato inválido")]
        public DateTime DataConsulta { get; set; }

        [Required(ErrorMessage = "A Especialidade é obrigatória.", AllowEmptyStrings = false)]
        [Display(Name = "Especialidade")]
        public int EspecialidadeId { get; set; }
        public IEnumerable<SelectListItem> Especialidades { get; set; }

        [Required]
        public int PacienteId { get; set; }
    }
}