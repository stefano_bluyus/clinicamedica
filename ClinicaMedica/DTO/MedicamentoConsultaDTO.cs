﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicaMedica.DTO
{
    public class MedicamentoConsultaDTO
    {
        public int ID { get; set; }
        public string NomeGenerico { get; set; }
        public string NomeComercial { get; set; }
        public string Posologia { get; set; }
    }
}