﻿using System;
using System.Collections.Generic;

namespace ClinicaMedica.DTO
{
    public class RelatorioHistoricoConsultaDTO
    {
        public string NomeMedico { get; set; }
        public string Especialidade { get; set; }
        public DateTime DataConsulta { get; set; }
    }

    public class RelatorioHistoricoConsultaPacienteDTO
    {
        public string NomePaciente { get; set; }
        public string NomeMae { get; set; }
        public DateTime DataNascimento { get; set; }

        public List<RelatorioHistoricoConsultaDTO> Consultas { get; set; }
    }
}