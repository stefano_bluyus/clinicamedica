﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicaMedica.DTO
{
    public class ConsultaExameDTO
    {
        public int Id { get; set; }
        public string Exame { get; set; }
    }
}