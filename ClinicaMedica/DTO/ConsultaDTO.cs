﻿using ClinicaMedica.Models;
using System;
using System.Collections.Generic;

namespace ClinicaMedica.DTO
{
    public class ConsultaDTO
    {
        public int Id { get; set; }
        public string NomePaciente { get; set; }
        public DateTime DataHoraConsulta { get; set; }
        public string Queixas { get; set; }

        public List<ConsultaCidDTO> Cids { get; set; }
        public List<ConsultaExameDTO> Exames { get; set; }
        public List<ConsultaMedicamentoDTO> Medicamentos { get; set; }
    }
}