﻿using System.Collections.Generic;

namespace ClinicaMedica.DTO
{
    public class RelatorioReceitaDTO: RelatorioConsultaDTO
    {
        public List<MedicamentoConsultaDTO> Medicamentos { get; set; }
    }
}