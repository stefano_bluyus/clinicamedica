﻿using ClinicaMedica.Models;
using System;
using System.Collections.Generic;

namespace ClinicaMedica.DTO
{
    public class ConsultaMedicamentoDTO
    {
        public int Id { get; set; }
        public string Medicamento { get; set; }
        public string Posologia { get; set; }
    }
}