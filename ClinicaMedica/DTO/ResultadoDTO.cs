﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ClinicaMedica.DTO
{
    public class ResultadoDTO
    {
        public int NumeroPedido { get; set; }

        [Display(Name = "Exame")]
        public string NomeExame { get; set; }

        [Display(Name = "Médico")]
        public string NomeMedico { get; set; }

        [Display(Name = "Situação")]
        public string Status { get; set; }

        [Display(Name = "Dt prescrição")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime DataPrescricao { get; set; }

        [Display(Name = "Dt aprovação")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime DataLiberacao { get; set; }

        public string Senha { get; set; }
    }
}