﻿using System;

namespace ClinicaMedica.DTO
{
    public class BuscaConsultaDTO
    {
        public DateTime Data { get; set; }
        public string NomeMedico { get; set; }
        public int Id { get; set; }
    }
}