﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicaMedica.DTO
{
    public class MedicamentoDTO
    {
        public int ID { get; set; }
        public string NomeComercial { get; set; }
        public string Descricao { get; set; }
    }
}