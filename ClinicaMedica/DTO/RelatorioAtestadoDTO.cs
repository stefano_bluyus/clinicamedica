﻿using System.Collections.Generic;

namespace ClinicaMedica.DTO
{
    public class RelatorioAtestadoDTO:RelatorioConsultaDTO
    {
        public List<TabelaDTO> Cids { get; set; }
    }
}