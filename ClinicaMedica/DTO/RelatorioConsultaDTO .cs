﻿using System;
using System.Collections.Generic;

namespace ClinicaMedica.DTO
{
    public class RelatorioConsultaDTO
    {
        public string NomePaciente { get; set; }
        public string NomeMedico { get; set; }
        public string CRM { get; set; }
        public DateTime DataConsulta { get; set; }
    }
}