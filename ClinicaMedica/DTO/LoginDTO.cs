﻿using System.ComponentModel.DataAnnotations;

namespace ClinicaMedica.DTO
{
    public class LoginDTO
    {
        [Required(ErrorMessage = "Informe o Login", AllowEmptyStrings = false)]
        [StringLength(10, MinimumLength = 3)]
        public string Login { get; set; }

        [Required(ErrorMessage = "Informe a Senha")]
        [DataType(DataType.Password)]
        public string Senha { get; set; }
    }
}