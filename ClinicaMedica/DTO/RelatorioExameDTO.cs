﻿using System;
using System.Collections.Generic;

namespace ClinicaMedica.DTO
{
    public class RelatorioExameDTO: RelatorioConsultaDTO
    {
        public List<ExameConsultaDTO> Exames { get; set; }
    }
}