﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClinicaMedica.DTO
{
    public class ExameConsultaDTO:ExameDTO
    {
        public string Login { get; set; }
        public string Senha { get; set; }
        public int NumeroPedido { get; set; }
    }
}