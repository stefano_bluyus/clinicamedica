﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ClinicaMedica.DTO
{
    public class AgendasDTO
    {
        public int IdMedico { get; set; }

        public string NomeMedico { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date)]
        public DateTime Data { get; set; }
    }  
}